USE odp ;

DROP PROCEDURE IF EXISTS validate_user ;

DELIMITER $$

CREATE PROCEDURE validate_user(username VARCHAR(25), password VARCHAR(50))
BEGIN
	CREATE TEMPORARY TABLE proc_temp(
		msg VARCHAR(255),
		username VARCHAR(25),
		role VARCHAR(10)
	) ;
	IF
		NOT EXISTS (SELECT * FROM users WHERE users.username = username)
	THEN
		INSERT INTO proc_temp VALUES("username invalid",username,NULL) ;
	ELSEIF EXISTS (SELECT * FROM users WHERE users.username = username AND users.password = password)
	THEN 
		INSERT INTO proc_temp VALUES("success",username,(SELECT role FROM users WHERE users.username = username AND users.password = password)) ;
	ELSE
		INSERT INTO proc_temp VALUES("invalid password",username,NULL) ;
	END IF ;
	
	SELECT * FROM proc_temp ;
	
	DROP TABLE proc_temp ;
END $$

DELIMITER ;
	