use odp ;

CREATE VIEW full_demo_details
(
	demo_id ,
	title ,
	description ,
	video_url ,
	resources 
)
AS
	SELECT 
		demos.demo_id, 
		demos.title,
		demo_details.description,
		demo_details.video_url,
		demo_details.resources
	FROM 
		demos
	LEFT JOIN demo_details ON demo_details.demo_id = demos.demo_id ;

CREATE VIEW demo_aws_details
(
	demo_id,
	ami,
	type,
	subnet_id
)
AS
	SELECT 
		demos.demo_id,
		aws.ami,
		aws.type,
		aws.subnet_id
	FROM demos
	LEFT JOIN aws ON aws.aws_id = demos.aws_id ;
	
CREATE VIEW demo_weaver_details
(
	demo_id,
	vnf_id,
	tenant_id,
	vnf_template_id
)
AS
	SELECT 
		demos.demo_id,
		weaver.vnf_id,
		weaver.tenant_id,
		weaver.vnf_template_id
	FROM demos
	LEFT JOIN weaver ON weaver.weaver_id = demos.weaver_id ;
	
