USE odp ;

# just insert two test users to the db
INSERT INTO users VALUES ('openet0','openet','cvt') ;
INSERT INTO users VALUES ('openet1','openet','non-cvt' ) ;

# insert test aws details
INSERT INTO aws(ami,type) VALUES ("test","m1.small") ;

# insert test weaver details
INSERT INTO weaver(vnf_id,tenant_id,vnf_template_id) VALUES ("test_vnf","default","test_template") ;


# insert our demo overviews
INSERT INTO demos VALUES ("demo_00",1,1,"BDPE","Test demo","/images/BDPE Use-Cases.png") ;
INSERT INTO demos VALUES ("demo_01",1,1,"CDM","Test demo","/images/CDM.png") ;
INSERT INTO demos VALUES ("demo_02",1,1,"Product Demo Suite","Test demo","/images/Product-Suite-Demo.png") ;
INSERT INTO demos VALUES ("demo_03",1,1,"App Visulisation","Test demo","/images/app-visualisation.png") ;
INSERT INTO demos VALUES ("demo_04",1,1,"ETSI","Test demo","/images/demo-etsi.png") ;
INSERT INTO demos VALUES ("demo_05",1,1,"PCC","Test demo","/images/demo-pcc.png") ;
INSERT INTO demos VALUES ("demo_06",1,1,"Rapid Time To Market","Test demo","/images/demo-rtom.png") ;
INSERT INTO demos VALUES ("demo_07",1,1,"VoLTE","Test demo","/images/demo-volte1.png") ;
INSERT INTO demos VALUES ("demo_08",1,1,"ECS Demo Platform","Test demo","/images/ecs_demo_platform.png") ;
INSERT INTO demos VALUES ("demo_09",1,1,"Insight e2e","Test demo","/images/insighte2e-demo.png") ;
INSERT INTO demos VALUES ("demo_10",1,1,"Policy VM","Test demo","/images/policy-vm-demo.png") ;
INSERT INTO demos VALUES ("demo_11",1,1,"NSI","Test demo","/images/nsi-demo.png") ;
INSERT INTO demos VALUES ("demo_12",1,1,"RTOM","Test demo","/images/rtom-demo.png") ;
INSERT INTO demos VALUES ("testvnfc",1,1,"TEST","Test demo","/images/BDPE Use-Cases.png") ;

# insert out demo details 
INSERT INTO demo_details VALUES ("demo_00", "blah", "blah blah", "blah blah blah" ) ;
INSERT INTO demo_details VALUES ("demo_01", "blah", "blah blah", "blah blah blah" ) ;
INSERT INTO demo_details VALUES ("demo_02", "blah", "blah blah", "blah blah blah" ) ;
INSERT INTO demo_details VALUES ("demo_03", "blah", "blah blah", "blah blah blah" ) ;
INSERT INTO demo_details VALUES ("demo_04", "blah", "blah blah", "blah blah blah" ) ;
INSERT INTO demo_details VALUES ("demo_05", "blah", "blah blah", "blah blah blah" ) ;
INSERT INTO demo_details VALUES ("demo_06", "blah", "blah blah", "blah blah blah" ) ;
INSERT INTO demo_details VALUES ("demo_07", "blah", "blah blah", "blah blah blah" ) ;
INSERT INTO demo_details VALUES ("demo_08", "blah", "blah blah", "blah blah blah" ) ;
INSERT INTO demo_details VALUES ("demo_09", "blah", "blah blah", "blah blah blah" ) ;
INSERT INTO demo_details VALUES ("demo_10", "blah", "blah blah", "blah blah blah" ) ;
INSERT INTO demo_details VALUES ("demo_11", "blah", "blah blah", "blah blah blah" ) ;
INSERT INTO demo_details VALUES ("demo_12", "blah", "blah blah", "blah blah blah" ) ;
INSERT INTO demo_details VALUES ("testvnfc", "blah", "blah blah", "blah blah blah" ) ;
