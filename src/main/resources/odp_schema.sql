CREATE SCHEMA IF NOT EXISTS odp ;

USE odp ;

CREATE TABLE IF NOT EXISTS users 
(
    username VARCHAR(25) PRIMARY KEY,
    password VARCHAR(50) NOT NULL,
    role VARCHAR(25) NOT NULL
) ENGINE = InnoDB ;

CREATE TABLE IF NOT EXISTS logs
(
    username VARCHAR(25) NOT NULL,
    timestamp DATETIME NOT NULL,
    action VARCHAR(255),
    vim VARCHAR(15),
    CONSTRAINT log_username
        FOREIGN KEY (username) 
            REFERENCES users(username) 
) ENGINE = InnoDB ;

CREATE TABLE IF NOT EXISTS aws
(
	aws_id INT AUTO_INCREMENT,
	ami VARCHAR(35) NOT NULL,
	type VARCHAR(15) NOT NULL,
	subnet_id VARCHAR(25) NOT NULL,
	PRIMARY KEY(aws_id)
) ENGINE = InnoDB ;

CREATE TABLE IF NOT EXISTS weaver
(
	weaver_id INT AUTO_INCREMENT,
	vnf_id VARCHAR(35) NOT NULL,
	tenant_id VARCHAR(35) NOT NULL,
	vnf_template_id VARCHAR(35) NOT NULL,
	vnfd_id VARHCAR(35) NOT NULL,
	PRIMARY KEY(weaver_id)
) ENGINE = InnoDB ;

CREATE TABLE IF NOT EXISTS demos
(
    demo_id VARCHAR(25) PRIMARY KEY,
    aws_id int NOT NULL,
    weaver_id int NOT NULL,
    title VARCHAR(50) NOT NULL,
    overview VARCHAR(100),
    image_path VARCHAR(255),
    CONSTRAINT aws_details
        FOREIGN KEY (aws_id) 
            REFERENCES aws(aws_id),
    CONSTRAINT weaver_details
        FOREIGN KEY (weaver_id) 
            REFERENCES weaver(weaver_id)
) ENGINE = InnoDB ;

CREATE TABLE IF NOT EXISTS snapshots
(
    snapshot_id VARCHAR(35) PRIMARY KEY,
    demo_id VARCHAR(25) NOT NULL,
    username VARCHAR(25) NOT NULL,
    created_date DATETIME NOT NULL,
    vim VARCHAR(15) NOT NULL,
    vm_image_name VARCHAR(100) NOT NULL,
    CONSTRAINT snapshot_demo
        FOREIGN KEY (demo_id) 
            REFERENCES demos(demo_id),
    CONSTRAINT snapshot_user
        FOREIGN KEY (username) 
            REFERENCES users(username)
) ENGINE = InnoDB ;

CREATE TABLE IF NOT EXISTS instances
(
    instance_id VARCHAR(25) PRIMARY KEY,
    demo_id VARCHAR(25) NOT NULL,
    username VARCHAR(25) NOT NULL,
    snapshot_id VARCHAR(25),
    instance_name VARCHAR(50) NOT NULL,
    created_date DATETIME NOT NULL,
    vim VARCHAR(15) NOT NULL,
    UUID VARCHAR(100),
    CONSTRAINT instance_demo
        FOREIGN KEY (demo_id) 
            REFERENCES demos(demo_id),
    CONSTRAINT is_a_snapshot
        FOREIGN KEY (snapshot_id)
            REFERENCES snapshots(snapshot_id),
    CONSTRAINT instance_owner
        FOREIGN KEY (username) 
            REFERENCES users(username)
) ENGINE = InnoDB ;

CREATE TABLE IF NOT EXISTS demo_details
(
    demo_id VARCHAR(25) NOT NULL,
    description TEXT(65535),
    video_url VARCHAR(50),
    resources VARCHAR(255)
    CONSTRAINT demo_id
        FOREIGN KEY (demo_id) 
            REFERENCES demos(demo_id)

) ENGINE = InnoDB ;

CREATE TABLE IF NOT EXISTS server_created_time
(
    OP_SERVER_UUID VARCHAR(100),
    SERVER_CREATED_TIME timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    STATUS VARCHAR(50)
) ENGINE = InnoDB ;
