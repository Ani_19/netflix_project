app.controller('demoViewer', function($scope, $http, $rootScope,$mdDialog,$sce) 
{
    $http({
        method: 'GET',
        url: "/demo"    
    }).then(function(response)
    {
        $scope.demoViewerData = response.data.demos ;

        $scope.curLow = 1 ; // Index of Left-most displayed tile in the carosel
        $scope.curHigh = calcViewableTiles() ; // Index of right-most displayed tile in the carosel

        // set the total number of tiles to be the total amount of data in our model
        $scope.totalTiles = $scope.demoViewerData.length ; 
        
    }) ;

    $http({
        method: 'GET',
        url: "/demoDetails"    
    }).then(function(response)
    {
        $scope.demoDetailsData = response.data.demoDetails ; 
        for(var i = 0; i < $scope.demoDetailsData.length; i++)
    	{
        	$scope.demoDetailsData[i].video_url = $sce.trustAsResourceUrl($scope.demoDetailsData[i].video_url) ;
        	if($scope.demoDetailsData[i].resources != null)
    		{
        		$scope.demoDetailsData[i].resources = $scope.demoDetailsData[i].resources.split(",");
    		}
    	}
    }) ;
    
    $http({
        method: 'GET',
        url: "/instance/"+sessionStorage.username   
    }).then(function(response)
    {
    	
    	$scope.usersInstances = response.data.instances ;

    }) ;

    // index of the tile selected in the carousel
    $scope.currentSelected = "" ; //if none selected set to -1

    // inital css class for a tile in the carousel
    $scope.demoTileClass = 'demo-tile'

    // when ever the window is resized we want to recalculate how many tiles can
    // fit on the screen
    window.addEventListener('resize',function(event)
    {
        $scope.curHigh = calcViewableTiles() ;
        $scope.$apply() ;
    }); 
    
    function userInstanceAsDemoMap(instanceData)
    {
    	var map = new Map() ;
    	for(var i = 0; i< instanceData.length; i++)
		{
    		map.set(instanceData[i].demo_Id, instanceData[i]) ;
		}
    	return map ;
    }
    
    /**
    * calcViewableTiles
    *
    * @return int: returns an integer representing the total number of tiles that can fit
    * flush in the carousel given the screens width on calling this function
    */
    function calcViewableTiles()
    {
        var tileWidth = 300 ; // tile width in demo-catalog.css
        var navButtonWidth = 100 ; // 20(margin*2) + 60(icon font) + 20(width)

        var windowWidth = window.innerWidth ;

        var totalFitableTiles = Math.floor( ( ( windowWidth - (navButtonWidth * 2) ) / tileWidth ) ) ; 

        console.log("Current Window width:"+windowWidth + " total tiles that can fit is:"+totalFitableTiles) ;

        return totalFitableTiles == 0 ? 1 : totalFitableTiles ;

    }
    
    /**
    * moveLeft 
    * if the current leftmost tile is not the first in the carosel the current leftmost and rightmost
    * tile to displays index is decreased
    */
    $scope.moveLeft = function() 
    {
      if($scope.curLow > 1)
      {
        $scope.curHigh--;
        $scope.curLow--;
      } 
    }

    /**
    * moveRight 
    * if the current rightmost tile is not the last in the carosel the current leftmost and rightmost
    * tile to displays index is increased
    */
    $scope.moveRight = function() 
    {
      if($scope.curHigh < $scope.totalTiles)
      {
          $scope.curHigh++;
          $scope.curLow++ ;
      }  
    }

    /**
    * showDetails
    * @param index: index of the tile that we want to show the details window for
    * 
    * if a tile is not alrady selected the tile with the index passed ins class is set demo-tile-selected
    * in order to use the css to indicate the tile is selected
    */
    $scope.showDetails = function(demo_id)
    {
        // on showing detils we want to remove set the tiles class as 
        // selected so that it is no longer enlarged on hover
        
        $scope.currentSelected = demo_id ;
        console.log($scope.currentSelected) ;
        $scope.demoTileClass = 'demo-tile-selected' ;
        
    }

    /**
    * closeDetails
    * sets the currentlySelected tile index to -1 and sets the css class back to default demo-tile class
    */
    $scope.closeDetails = function()
    {
        $scope.currentSelected = "" ;
        $scope.demoTileClass = 'demo-tile' ;
    }
    
    $scope.launchDemo = function(demoId)
    {
    	var request = {} ;
         	
        	request.username = sessionStorage.username;
        	$scope.status = "Launching demo on Openet private cloud";
        			
        	$http.post("/instance/"+demoId, request).then(function(response)
        	{
    	    	$scope.status = response.data.message ;
    		}, function(response)
    		{
    			//$scope.status = response.data.message ;
    			$window.open(response.data.message);
    		});	
    	
    }
    
	$scope.confirmLaunch = function(ev,demoId) {
	    
	    var confirm = $mdDialog.confirm()
	          .title('Launch')
	          .textContent('Please confirm that you wish to launch this demo to Openet Private Cloud')
	          .targetEvent(ev)
	          .ok('Yes')
	          .cancel('no');
	
	    $mdDialog.show(confirm).then(function() {
	    	$scope.launchDemo(demoId) ;
	    });
	  };
      
	$scope.confirmTermination = function(ev, demoId) {
	     
	var confirm = $mdDialog.confirm()
			.title('Delete')
			.textContent('Please confirm that you wish to terminate this demo')
			.targetEvent(ev)
			.ok('Yes')
			.cancel('no');
	
	$mdDialog.show(confirm).then(function() {
		$scope.terminateDemo(demoId) ;
	  });
	};
	
	$scope.confirmLaunchNew = function(ev, demoId) {
	     
		var confirm = $mdDialog.confirm()
				.title('LaunchNew')
				.textContent('Please confirm that you wish to launch new demo on Openet Private Cloud ')
				.targetEvent(ev)
				.ok('Yes')
				.cancel('no');
		
		$mdDialog.show(confirm).then(function() {
			$scope.launchNewDemo(demoId) ;
		  });
		};
	
		$scope.launchNewDemo = function(demoId)
		{
    	var request = {} ;
         	
        	request.username = sessionStorage.username;
			$scope.status = "Launching New Demo on Openet Private Cloud" ;
	    	$http.post("/launchnew/"+demoId, request).then(function(response)
	    	{
		    	$scope.status = response.data.message ;
			}, function(response)
			{
				$scope.status = response.data.message ;
			});
		}
	
	$scope.confirmRelaunch = function(ev, demoId){
		var confirm = $mdDialog.confirm()
			.title('Relaunch')
			.textContent('Please confirm that you wish to relaunch Demo on Openet Private Cloud')
			.targetEvent(ev)
			.ok('Yes')
			.cancel('no');
		
		$mdDialog.show(confirm).then(function() {
			$scope.relaunchDemo(demoId) ;
		  });
	};
	
	$scope.relaunchDemo = function(demoId)
	{
    	var request = {} ;
     	
    	request.username = sessionStorage.username;
    	$scope.status = "Relaunching demo on Openet Private Cloud";
    			
    	$http.post("/relaunch/"+demoId, request).then(function(response)
    	{
	    	$scope.status = response.data.message ;
		}, function(response)
		{
			//$scope.status = response.data.message ;
			$window.open(response.data.message);
		});	
	}
	

  
	$scope.terminateDemo = function(demoId)
	{
		$scope.status = "Terminating demo" ;
    	$http.delete("/instance/"+demoId+"/"+sessionStorage.username ).then(function(response)
    	{
	    	$scope.status = response.data.message ;
		}, function(response)
		{
			$scope.status = response.data.message ;
		});
	}
    
});