app.factory('ODPAuth', function($http)
{
	var odpAuth = {} ;
	odpAuth.login = function(credentials)
	{
		return $http.post(
			"/user",
			credentials
		).then(function(response)
		{	
			sessionStorage.username = response.data.username ;
			sessionStorage.role = response.data.role ;
	    	return response.data ;
		});
	}
	return odpAuth ;
});

app.controller('loginCtrl', function($scope, $rootScope, ODPAuth) 
{
	$scope.status = "" ;
	console.log(sessionStorage.username) ;
	$scope.credentials = { 
		username: '',
		password: ''
	};
	
	$scope.login = function(credentials) 
	{
		ODPAuth.login(credentials).then(function (user) 
		{
			$rootScope.$broadcast("login-success");
			window.location.href="demoCatalog.html#main" ;
 		}, function () 
		{
 			$scope.status = "Invalid Login" ;
			$rootScope.$broadcast("login-failed");
		});
  };
  
});