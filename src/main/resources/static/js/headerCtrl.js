app.controller('headerCtrl', function($scope,$timeout) 
{
	$scope.currentUser = sessionStorage.username ;
	$scope.currentUserRole = sessionStorage.role ; 
	
	console.log($scope.currentUserRole) ;
	console.log($scope.currentUser) ;
	
	$scope.logout = function()
	{
		sessionStorage.clear();
		window.location.href="/login.html" ;
	}
	
	$scope.redirect = function(page)
	{
		window.location.href=page ;
	}
	
	//if the user is inactive for 5 mins log them out
	$timeout(function(){
		$scope.logout() ;
	},300000) ;
	
});

