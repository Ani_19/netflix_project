app.controller('logsctrl', function($scope,$http) 
{   
    
    $http({
        method: 'GET',
        url: "log"    
    }).then(function(response){
        $scope.logs = response.data.logs ;
    }) ;

});


app.controller('settingsCtrl',function($scope,$http)
{
    $http({
        method: 'GET',
        url: "/setting/prefered vim"    
    }).then(function(response){
        $scope.preferedVimSetting = response.data.options ;
    }) ;

}) ;