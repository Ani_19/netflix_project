package com.openet.odp;

import org.springframework.boot.SpringApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import org.springframework.boot.autoconfigure.SpringBootApplication;


/**
 * 
 * @author eoin murphy
 *
 */
@SpringBootApplication
@EnableSwagger2
@PropertySource(value="classpath:/resources/application.properties")
public class Application {

	public static void main(String [] args)
	{
		SpringApplication.run(Application.class, args) ;
	}

	@Bean 
	public Docket docket()
	{
		return new Docket(DocumentationType.SWAGGER_2)
				.apiInfo(apiInfo());
	}
	
	public ApiInfo apiInfo()
	{
		return new ApiInfoBuilder()
				.title("Openet Demo Platorm API Swagger2 Doc")
				.version("0.1")
				.build() ;
	}
}
