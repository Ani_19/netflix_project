package com.openet.odp.spring.controllers;

import javax.inject.Inject;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.openet.odp.spring.model.Setting;
import com.openet.odp.service.SettingService;
import com.openet.odp.spring.model.SettingsList;
import com.openet.odp.spring.response.StatusResponse;


import io.swagger.annotations.ApiOperation;

/**
 * 
 * @author eoin murphy
 *
 */
@RestController
public class SetttingController {

	@Inject
	private SettingService settingService ;
	
	/**
	 * 
	 * @return
	 */
	@ApiOperation(value="getAllSettings")
	@RequestMapping(method=RequestMethod.GET, value="/setting")
	public ResponseEntity<SettingsList> setting()
	{
		SettingsList response = settingService.getAllSettings() ;
		
		return new ResponseEntity<SettingsList>(response,HttpStatus.OK) ;
	}
	
	/**
	 * 
	 * @param setting_name
	 * @return
	 */
	@ApiOperation(value = "getSetting") 
	@RequestMapping(method = RequestMethod.GET, value="/setting/{setting_name}")
	public ResponseEntity<Setting> setting(@PathVariable String setting_name)
	{
		
		Setting response = settingService.getSetting(setting_name) ;
		
		return new ResponseEntity<Setting>(response,HttpStatus.OK) ;
	}
	
	//TODO: at the moment there are no checks made when setting a setting it will just overwrite the file 
	// with what ever is passed in.
	/**
	 * 
	 * @param request
	 * @return
	 */
	@ApiOperation(value = "setSetting") 
	@RequestMapping(method = RequestMethod.POST, value="/setting")
	public ResponseEntity<StatusResponse> setting(@RequestBody Setting request)
	{
		settingService.setSetting(request);
		
		StatusResponse response = new StatusResponse(
				HttpStatus.OK.value(), 
				HttpStatus.OK.getReasonPhrase(),
				"Updated setting with new configuration");
		return new ResponseEntity<StatusResponse>(response,HttpStatus.OK) ;
		
	}
}
