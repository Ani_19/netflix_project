package com.openet.odp.spring.controllers;

import java.sql.SQLException;

import javax.inject.Inject;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.openet.odp.service.UserService;
import com.openet.odp.spring.model.User ;
import com.openet.odp.spring.response.LoginResponse;

import io.swagger.annotations.ApiOperation;

@RestController
public class UserControler {
	
	@Inject
	private UserService userService ;
	
	@ApiOperation(value="login")
	@RequestMapping(method=RequestMethod.POST,value="/user")
	public ResponseEntity<LoginResponse> login(@RequestBody User user) throws SQLException
	{
		
		LoginResponse response = userService.login(
				user.getUsername(), user.getPassword()) ;
		
		//if the user was not validated on the system return a forbiddden response
		if(response.getValid() == false)
		{
			return new ResponseEntity<LoginResponse>(response,HttpStatus.FORBIDDEN) ;
		}
		
		return new ResponseEntity<LoginResponse>(response,HttpStatus.OK) ;
	}
}
