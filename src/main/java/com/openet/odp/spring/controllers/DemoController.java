package com.openet.odp.spring.controllers;

import java.sql.SQLException;

import javax.inject.Inject;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.openet.odp.service.DemoService;
import com.openet.odp.spring.model.Demo;
import com.openet.odp.spring.response.DemoResponse;

import io.swagger.annotations.ApiOperation;

@RestController
public class DemoController {
	
	@Inject
	private DemoService demoService ;
	
	@ApiOperation(value="getAllDemos")
	@RequestMapping(method=RequestMethod.GET,value="/demo")
	public ResponseEntity<DemoResponse> demo() throws SQLException //TODO: proper exceptions 
	{
		DemoResponse response = null ;	
		
		response = new DemoResponse(demoService.getAllDemos()) ;
		
		return new ResponseEntity<DemoResponse>(response, HttpStatus.OK) ;
	}
	
	@ApiOperation(value="getDemo")
	@RequestMapping(method=RequestMethod.GET,value="/demo/{demo_id}")
	public ResponseEntity<Demo> demo(@PathVariable String demo_id) throws SQLException //TODO: proper exceptions 
	{
			
		Demo response = demoService.getDemo(demo_id) ;
		
		return new ResponseEntity<Demo>(response, HttpStatus.OK) ;
	}
	
}
