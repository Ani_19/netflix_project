package com.openet.odp.spring.controllers;

import java.sql.SQLException;

import javax.inject.Inject;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;

import com.openet.odp.service.DemoDetailsService;
import com.openet.odp.spring.model.DemoDetails;
import com.openet.odp.spring.response.DemoDetailsResponse;

@RestController
public class DemoDetailsController {

	@Inject
	private DemoDetailsService demoDetailsService ;
	
	@ApiOperation( value="getAllDemoDetails")
	@RequestMapping(method=RequestMethod.GET,value="/demoDetails")
	public ResponseEntity<DemoDetailsResponse> getAllDemoDetails() throws SQLException
	{
		DemoDetailsResponse response = new DemoDetailsResponse(demoDetailsService.getAllDemoDetails()) ;
		
		return new ResponseEntity<DemoDetailsResponse>(response,HttpStatus.OK) ;
	}
	
	@ApiOperation( value="getDemoDetails")
	@RequestMapping(method=RequestMethod.GET,value="/demoDetails/{demo_id}")
	public ResponseEntity<DemoDetails> getDemoDetails(@PathVariable String demo_id) throws SQLException
	{
		DemoDetails response = demoDetailsService.getDemoDetails(demo_id) ;
		
		return new ResponseEntity<DemoDetails>(response,HttpStatus.OK) ;
	}
}
