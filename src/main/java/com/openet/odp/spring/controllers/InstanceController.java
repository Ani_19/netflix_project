package com.openet.odp.spring.controllers;

import java.sql.SQLException;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping ;
import org.springframework.web.bind.annotation.RestController ;
import org.springframework.web.bind.annotation.RequestMethod ;

import com.openet.odp.service.InstanceService;
import com.openet.odp.service.OpenstackService;
import com.openet.odp.spring.request.DemoRequest;
import com.openet.odp.spring.response.InstanceResponse;
import com.openet.odp.spring.response.StatusResponse;

import io.swagger.annotations.ApiOperation;


/**
 * 
 * @author eoin murphy
 *
 */
@RestController
public class InstanceController {
	
	
	@Inject
	private InstanceService instanceService;
		
	private final Logger logger = LoggerFactory.getLogger(InstanceController.class);
	
	
	
	/**
	 * 
	 * @return
	 * @throws SQLException 
	 */
	@ApiOperation(value="getAllInstances")
	@RequestMapping(method=RequestMethod.GET,value="/instance")
	public ResponseEntity<InstanceResponse> getAllInstances() throws SQLException
	{
		
		InstanceResponse response = new InstanceResponse(instanceService.getAllInstances()) ;
		
		return new ResponseEntity<InstanceResponse>(response, HttpStatus.OK) ;
	}
	
	/**
	 * 
	 * @param demo_id
	 * @return
	 * @throws SQLException 
	 */
	@ApiOperation(value="getUsersInstances")
	@RequestMapping( method=RequestMethod.GET, value="/instance/{username}")
	public ResponseEntity<InstanceResponse> getUsersInstances(@PathVariable String username) throws SQLException
	{
		
		InstanceResponse response = new InstanceResponse(instanceService.getUsersInstances(username)) ;
		return new ResponseEntity<InstanceResponse>(response, HttpStatus.OK) ;
	}
	
	@ApiOperation(value="getDemoInstances")
	@RequestMapping( method=RequestMethod.GET, value="/instance/{demo_id}/{username}")
	public ResponseEntity<InstanceResponse> getUserDemoInstances(@PathVariable String demo_id, @PathVariable String username) throws SQLException
	{
		
		InstanceResponse response = new InstanceResponse(instanceService.getUserDemoInstances(demo_id, username)) ;
		return new ResponseEntity<InstanceResponse>(response, HttpStatus.OK) ;
	}
	
	/**
	 * 
	 * @param demo_id
	 * @param request
	 * @return
	 * @throws SQLException 
	 */
	@ApiOperation(value="launchDemo")
	@RequestMapping( method=RequestMethod.POST, 
			value="/instance/{demo_id}", 
			headers = "content-type=application/json")
	public ResponseEntity<StatusResponse> launchInstance( @PathVariable String demo_id , 
			@RequestBody DemoRequest request) throws SQLException
	{		
		logger.info("DEMO_ID: "+demo_id);
		StatusResponse response = null ;
		InstanceService instanceService = new InstanceService();
		//OpenstackService openstackSerrvice = new OpenstackService();

		if(!request.isValid())
		{
			response = new StatusResponse(
					HttpStatus.BAD_REQUEST.value(), 
					HttpStatus.BAD_REQUEST.getReasonPhrase(),
					"Missing request value!") ;
			
			return new ResponseEntity<StatusResponse>(response, HttpStatus.BAD_REQUEST) ;
		}

		 String consoleURL = instanceService.launchNewDemo(demo_id, request.getUsername());		
		if(consoleURL == null)
		{
			response = new StatusResponse(
					HttpStatus.INTERNAL_SERVER_ERROR.value(), 
					HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase(),
					"Failed to instantiate instance") ;
			
			return new ResponseEntity<StatusResponse>(response,HttpStatus.INTERNAL_SERVER_ERROR) ;
		}
		
		response = new StatusResponse(
				HttpStatus.CREATED.value(), 
				HttpStatus.CREATED.getReasonPhrase(),
				"Instance URL  "+consoleURL) ;
		
		return new ResponseEntity<StatusResponse>(response,HttpStatus.CREATED) ;
	}
	
	
	@ApiOperation(value="relaunchDemo")
	@RequestMapping( method=RequestMethod.POST, 
			value="/relaunch/{demo_id}", 
			headers = "content-type=application/json")
	public ResponseEntity<StatusResponse> relaunchInstance( @PathVariable String demo_id , 
			@RequestBody DemoRequest request) throws SQLException
	{		
		logger.info("DEMO_ID: "+demo_id);
		StatusResponse response = null ;
		InstanceService instanceService = new InstanceService();

		if(!request.isValid())
		{
			response = new StatusResponse(
					HttpStatus.BAD_REQUEST.value(), 
					HttpStatus.BAD_REQUEST.getReasonPhrase(),
					"Missing request value!") ;
			
			return new ResponseEntity<StatusResponse>(response, HttpStatus.BAD_REQUEST) ;
		}

		 String consoleURL = instanceService.relaunchDemo(demo_id, request.getUsername());		
		if(consoleURL == null)
		{
			response = new StatusResponse(
					HttpStatus.INTERNAL_SERVER_ERROR.value(), 
					HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase(),
					"Failed to instantiate instance") ;
			
			return new ResponseEntity<StatusResponse>(response,HttpStatus.INTERNAL_SERVER_ERROR) ;
		}
		
		response = new StatusResponse(
				HttpStatus.CREATED.value(), 
				HttpStatus.CREATED.getReasonPhrase(),
				"Instance URL  "+consoleURL) ;
		
		return new ResponseEntity<StatusResponse>(response,HttpStatus.CREATED) ;
	}
	
	
	@ApiOperation(value="launchnewDemo")
	@RequestMapping( method=RequestMethod.POST, 
			value="/launchnew/{demo_id}", 
			headers = "content-type=application/json")
	public ResponseEntity<StatusResponse> launchNewInstance( @PathVariable String demo_id , 
			@RequestBody DemoRequest request) throws SQLException
	{		
		logger.info("DEMO_ID: "+demo_id);
		StatusResponse response = null ;
		InstanceService instanceService = new InstanceService();

		if(!request.isValid())
		{
			response = new StatusResponse(
					HttpStatus.BAD_REQUEST.value(), 
					HttpStatus.BAD_REQUEST.getReasonPhrase(),
					"Missing request value!") ;
			
			return new ResponseEntity<StatusResponse>(response, HttpStatus.BAD_REQUEST) ;
		}

		 String consoleURL = instanceService.reLaunchNewDemo(demo_id, request.getUsername());		
		if(consoleURL == null)
		{
			response = new StatusResponse(
					HttpStatus.INTERNAL_SERVER_ERROR.value(), 
					HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase(),
					"Failed to instantiate instance") ;
			
			return new ResponseEntity<StatusResponse>(response,HttpStatus.INTERNAL_SERVER_ERROR) ;
		}
		
		response = new StatusResponse(
				HttpStatus.CREATED.value(), 
				HttpStatus.CREATED.getReasonPhrase(),
				"Instance URL  "+consoleURL) ;
		
		return new ResponseEntity<StatusResponse>(response,HttpStatus.CREATED) ;
	}
	
	@ApiOperation(value = "deleteDemo")
	@RequestMapping( method = RequestMethod.DELETE,
			value="/instance/{demo_id}/{username}")
	public ResponseEntity<StatusResponse> deleteDemo(@PathVariable String demo_id,
			@PathVariable String username) throws SQLException
	{
		
		boolean deleted = instanceService.deleteDemo(demo_id, username) ;
		
		if(deleted == false)
		{
			StatusResponse response = new StatusResponse(
					HttpStatus.INTERNAL_SERVER_ERROR.value(), 
					HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase(),
					"Failed to delete instance") ;
			
			return new ResponseEntity<StatusResponse>(response,HttpStatus.INTERNAL_SERVER_ERROR) ;
		}
		StatusResponse response = new StatusResponse(
				HttpStatus.CREATED.value(), 
				HttpStatus.CREATED.getReasonPhrase(),
				"Deleted instance of demo "+demo_id+ " for user "+ username) ; 

		
		return new ResponseEntity<StatusResponse>(response,HttpStatus.CREATED) ;
		
	}
}
