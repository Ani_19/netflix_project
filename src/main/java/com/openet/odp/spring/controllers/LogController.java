package com.openet.odp.spring.controllers;

import java.sql.SQLException;

import javax.inject.Inject;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.openet.odp.service.LogService;
import com.openet.odp.spring.response.LogResponse;

import io.swagger.annotations.ApiOperation;

/**
 * 
 * @author eoin murphy
 *
 */
@RestController
public class LogController {

	@Inject
	private LogService logService ;
	
	/**
	 * 
	 * @param min
	 * @param max
	 * @return
	 * @throws SQLException 
	 */
	@ApiOperation(value="getAllLogs")
	@RequestMapping(method=RequestMethod.GET,value="/log")
	public ResponseEntity<LogResponse> log(@RequestParam(defaultValue="0") int offset, 
			@RequestParam(defaultValue="25") int count) throws SQLException
	{
			
		LogResponse response = new LogResponse(logService.getAllLogs(offset, count)) ;
		
		return new ResponseEntity<LogResponse>(response, HttpStatus.OK) ;
	}
	
	/**
	 * 
	 * @param min
	 * @param max
	 * @param username
	 * @return
	 * @throws SQLException 
	 */
	@ApiOperation(value="getUserLogs")
	@RequestMapping(method=RequestMethod.GET, value="/log/{username}")
	public ResponseEntity<LogResponse> log(@RequestParam(defaultValue="0") int offset, 
			@RequestParam(defaultValue="25") int count,
			@PathVariable String username) throws SQLException
	{
		
		LogResponse response = new LogResponse(logService.getUserLogs(offset, count, username)) ;
		
		return new ResponseEntity<LogResponse>(response, HttpStatus.OK) ;
	}
}
