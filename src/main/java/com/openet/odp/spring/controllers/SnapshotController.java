package com.openet.odp.spring.controllers;

import java.sql.SQLException;

import javax.inject.Inject;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.openet.odp.service.SnapshotService;
import com.openet.odp.spring.response.SnapshotResponse;
import com.openet.odp.spring.response.StatusResponse;

import io.swagger.annotations.ApiOperation;

/**
 * 
 * @author eoin murphy
 *
 */
@RestController
public class SnapshotController {

	@Inject
	private SnapshotService snapshotService ;
	
	/**
	 * 
	 * @return
	 * @throws SQLException 
	 */
	@ApiOperation( value="getAllSnapshots")
	@RequestMapping(method=RequestMethod.GET,value="/snapshot")
	public ResponseEntity<SnapshotResponse> getAllSnapshot() throws SQLException
	{	
		SnapshotResponse response = new SnapshotResponse(snapshotService.getAllSnapshots()) ;
		
		return new ResponseEntity<SnapshotResponse>(response,HttpStatus.OK) ;
	}
	
	
	/**
	 * 
	 * @param username
	 * @return
	 * @throws SQLException 
	 */
	@ApiOperation( value="getUsersSnapshots")
	@RequestMapping(method=RequestMethod.GET,value="/snapshot/{username}")
	public ResponseEntity<SnapshotResponse> getUsersSnapshots(@PathVariable String username) throws SQLException
	{			
		SnapshotResponse response = new SnapshotResponse(snapshotService.getUsersSnapshots(username)) ;
		
		return new ResponseEntity<SnapshotResponse>(response,HttpStatus.OK) ;
	}
	
	@ApiOperation (value = "launchSnapshot")
	@RequestMapping(method=RequestMethod.POST, value="/snapshot/{username}/{demo_id}")
	public ResponseEntity<StatusResponse> launchSnapshot(@PathVariable String username, @PathVariable String demo_id)
	{
		return null;
		
	}
	
	@ApiOperation (value = "deleteSnapshot")
	@RequestMapping(method=RequestMethod.DELETE, value="/snapshot/{username}/{demo_id}")
	public ResponseEntity<StatusResponse> deleteSnapshot(@PathVariable String username, @PathVariable String demo_id)
	{
		return null;
		
	}
}
