package com.openet.odp.spring.response;

import java.util.List;

import com.openet.odp.spring.model.Snapshot;

/**
 * 
 * @author eoin Murphy
 *
 */
public class SnapshotResponse {
	
private final List<Snapshot> snapshot ;
	
	/**
	 * 
	 * @param response
	 */
	public SnapshotResponse(List<Snapshot> snapshot)
	{
		this.snapshot = snapshot ;
	}
	
	public List<Snapshot> getSnapshot()
	{
		return snapshot ;
	}
}
