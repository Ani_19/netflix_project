package com.openet.odp.spring.response;

import java.util.List;

import com.openet.odp.spring.model.Demo;

public class DemoResponse {

	private final List<Demo> demos ;
	
	public DemoResponse(List<Demo> demos)
	{
		this.demos = demos ;
	}
	
	public List<Demo> getDemos()
	{
		return demos ;
	}
}
