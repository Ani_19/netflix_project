package com.openet.odp.spring.response;

public class LoginResponse {
	
	private final String username ;
	private final boolean valid ;
	private final String message ;
	private final String role ;
	
	public LoginResponse(String username, boolean valid, String message,String role)
	{
		this.username = username ;
		this.valid = valid ;
		this.message  = message ;
		this.role = role ;
	}
	
	public String getUsername()
	{
		return username ;
	}
	
	public boolean getValid()
	{
		return valid ;
	}
	
	public String getMessage()
	{
		
		return message ;
	}
	
	public String getRole()
	{
		return role ;
	}
}
