package com.openet.odp.spring.response;

/**
 * POJ to represt and response to return when a user makes a post request
 * @author eoin
 *
 */
public class StatusResponse {

	private final int code ;
	private final String status ;
	private final String message ;
	
	/**
	 * 
	 * @param code
	 * @param status
	 * @param message
	 */
	public StatusResponse(int code, String status, String message)
	{
		this.code 		= code ;
		this.status		= status ;
		this.message 	= message ;
	}
	
	public int getCode()
	{
		return code ;
	}
	
	public String getStatus()
	{
		return status ;
	}
	
	public String getMessage()
	{
		return message ;
	}
}
