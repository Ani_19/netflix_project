package com.openet.odp.spring.response;

import java.util.List;

import com.openet.odp.spring.model.Log;

/**
 * 
 * @author eoin
 *
 */
public class LogResponse {

	private final List<Log> logs ;
	
	/**
	 * 
	 * @param response
	 */
	public LogResponse(List<Log> logs)
	{
		this.logs = logs ;
	}
	
	/**
	 * 
	 * @return
	 */
	public List<Log> getLogs()
	{
		return logs ;
	}
}
