package com.openet.odp.spring.response;

import java.util.List;

import com.openet.odp.spring.model.Instance;

/**
 * POJ to represent a list of demo objects in response to 
 * a users request for a list of all launched demos
 * @author eoin
 *
 */
public class InstanceResponse {
	
	private final List<Instance> instances ;
	
	/**
	 * 
	 * @param response
	 */
	public InstanceResponse(List<Instance> instances)
	{
		this.instances = instances ;
	}
	
	/**
	 * 
	 * @return
	 */
	public List<Instance> getInstances()
	{
		return instances ;
	}
}
