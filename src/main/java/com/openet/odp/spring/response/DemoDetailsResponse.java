package com.openet.odp.spring.response;

import java.util.List;

import com.openet.odp.spring.model.DemoDetails ;

public class DemoDetailsResponse {

	
	private final List<DemoDetails> demoDetails ;
	
	public DemoDetailsResponse(List<DemoDetails> demoDetails)
	{
		this.demoDetails = demoDetails ;
	}
	
	public List<DemoDetails> getDemoDetails()
	{
		return demoDetails ;
	}
}
