package com.openet.odp.spring.model;

public class DemoDetails {

	private final String demo_id ;
	private final String description ;
	private final String video_url ;
	private final String resources ;
	private final String title ;
	
	public DemoDetails(String demo_id, String description, String video_url, 
			String resources,String title)
	{
		this.demo_id 		= demo_id ;
		this.description 	= description ;
		this.video_url 		= video_url ;
		this.resources 		= resources ;
		this.title			= title ;
	}
	
	public String getDemo_id()
	{
		return demo_id ;
	}
	
	public String getDescription()
	{
		return description ;
	}
	
	public String getVideo_url()
	{
		return video_url ;
	}
	
	public String getResources()
	{
		return resources ;
	}
	
	public String getTitle()
	{
		return title ;
	}
}
