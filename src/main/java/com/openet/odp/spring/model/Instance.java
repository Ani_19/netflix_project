package com.openet.odp.spring.model ;

/**
 * 
 * @author eoin murphy
 *
 */
public class Instance 
{
	private final String instance_id ;
    private final String demo_id ;
    private final String snapshot_id ;
    private final String create_date ;
    private final String user ;
    private final String vim ;

    /**
     * 
     * @param instance_id
     * @param demo_id
     * @param snapshot_id
     * @param create_date
     * @param user
     * @param vim
     */
    public Instance( String instance_id, String demo_id, String snapshot_id, 
    		String create_date, String user, String vim)
    {
    	this.instance_id 	= instance_id ;
        this.demo_id        = demo_id ;
        this.snapshot_id 	= snapshot_id ;
        this.create_date    = create_date ;
        this.user           = user ;
        this.vim            = vim ;
    }
    
    public String getInstance_Id()
    {
    	return instance_id ;
    }

    public String getDemo_Id()
    {
        return demo_id ;
    }

    public String getCreate_date()
    {
        return create_date ;
    }

    public String getUser()
    {
        return user ;
    }

    public String getVim()
    {
        return vim ;
    }
    
    public String getSnapshot_id()
    {
    	return snapshot_id ;
    }
}