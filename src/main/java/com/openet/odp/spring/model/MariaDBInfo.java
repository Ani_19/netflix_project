package com.openet.odp.spring.model;

public class MariaDBInfo {
	public  String jdbcUrl;
	public  String  user;
	public  String  password;
	
	public String getjdbcUrl(){
		return jdbcUrl;
	}
	public String getUser(){
		return user;
	}
	public String getPassword(){
		return password;
	}
}
