package com.openet.odp.spring.model;

/**
 * 
 * @author eoin murphy
 *
 */
public class Snapshot {
	
	private final String snapshot_id ;
	private final String demo_id ;
    private final String create_date ;
    private final String user ;
    private final String vim ;
    private final String vm_image_name ;
    

    /**
     * 
     * @param snapshot_id
     * @param demo_id
     * @param create_date
     * @param user
     * @param vim
     */
    public Snapshot(String snapshot_id, String demo_id, String create_date, 
    		String user, String vim, String vm_image_name)
    {
    	this.snapshot_id	= snapshot_id ;
        this.demo_id        = demo_id ;
        this.create_date    = create_date ;
        this.user           = user ;
        this.vim            = vim ;
        this.vm_image_name 	= vm_image_name ;

    }
    
    public String getSnapshot_id()
    {
    	return snapshot_id ;
    }

    public String getDemo_Id()
    {
        return demo_id ;
    }

    public String getCreate_date()
    {
        return create_date ;
    }

    public String getUser()
    {
        return user ;
    }

    public String getVim()
    {
        return vim ;
    }
    
    public String getVm_image_name()
    {
    	return vm_image_name ;
    }
}
