package com.openet.odp.spring.model;

public class OpenstackInfo {
	public String ipAddress;
	public String  port;
	public String tenantId;
	public String password;
	public String user; 
	public String apiVersion;
	
	public String getIpAddress(){
		return ipAddress;
	}
	public String getPort(){
		return port;
	}
	public String getTenantId(){
		return tenantId;
	}
	public String getPassword(){
		return password;
	}
	public String getUser(){
		return user;
	}
	public String getApiVersion(){
		return apiVersion;
	}
}
