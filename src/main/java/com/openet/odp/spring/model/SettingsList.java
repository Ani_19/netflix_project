package com.openet.odp.spring.model;

import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SettingsList {

	private List<Setting> settings ;
	
	SettingsList(@JsonProperty(value="settings") List<Setting> settings)
	{
		this.settings = settings ;
	}

	public List<Setting> getSettings()
	{
		return settings ;
	}

	public Setting getSetting(String name)
	{
		for(Setting setting : settings)
		{
			if(setting.getName().equals(name))
				return setting ;
		}
		
		return null ;
	}
	
	public void setSettings(List<Setting> settings)
	{
		this.settings = settings ;
	}
	
	/**
	 * overwrites the setting entry in the settings list with the setting object
	 * passed as the param
	 * @param setting
	 * @return true if setting object found and overwrited
	 * false if no object found
	 */
	public boolean setSetting(Setting setting)
	{
		for(int i = 0; i < settings.size(); i ++)
		{
			if(settings.get(i).getName().equals(setting.getName()))
			{
				settings.set(i, setting) ;
				return true ;
			}
		}
		
		return false ;
	}
}

