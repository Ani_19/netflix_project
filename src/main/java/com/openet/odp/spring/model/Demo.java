package com.openet.odp.spring.model;

public class Demo {
	
	private final String demo_id ;
	private final String title ;
	private final String description ;
	private final String image_path ;
	
	public Demo(String demo_id, String title, 
			String description, String image_path)
	{
		this.demo_id		= demo_id ;
		this.title 			= title ;
		this.description 	= description ;
		this.image_path 	= image_path ;
	}
	public String getDemo_id()
	{
		return demo_id ;
	}
	
	public String getTitle()
	{
		return title ;
	}
	
	public String getDescription()
	{
		return description ;
	}
	
	public String getImage_path()
	{
		return image_path ;
	}
}
