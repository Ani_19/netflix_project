package com.openet.odp.spring.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class User {

	private final String username ;
	private final String password ;
	
	
	public User(@JsonProperty("username") String username,
			@JsonProperty("password") String password)
	{
		this.username = username ;
		this.password = password ;
	}
	
	public String getUsername()
	{
		return username ;
	}
	
	public String getPassword()
	{
		return password ;
	}
}
