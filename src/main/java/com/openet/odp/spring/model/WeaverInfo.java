package com.openet.odp.spring.model;

public class WeaverInfo {
	public  String weaverIpAddress;
	public String  weaverPort;
	
	public String getWeaverIpAddress(){
		return weaverIpAddress;
	}
	public String getWeaverPort(){
		return weaverPort;
	}
	
}
