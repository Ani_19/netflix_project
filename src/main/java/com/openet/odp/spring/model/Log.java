package com.openet.odp.spring.model;

/**
 * 
 * @author eoin murphy
 *
 */
public class Log {

		private final String timestamp ;
		private final String user ;
		private final String action ;
		private final String vim ;
		
		/**
		 * 
		 * @param timestamp
		 * @param user
		 * @param action
		 * @param vim
		 */
		public Log(String user, String timestamp,
				String action, String vim )
		{
			this.timestamp 	= timestamp ;
			this.user 		= user ;
			this.action 	= action ;
			this.vim 		= vim ;
		}
		
		public String getTimestamp()
		{
			return timestamp ;
		}
		
		public String getUser()
		{
			return user ;
		}
		
		public String getAction()
		{
			return action ;
		}
		
		public String getVim()
		{
			return vim ;
		}
}
