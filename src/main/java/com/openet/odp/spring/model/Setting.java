package com.openet.odp.spring.model;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author eoin murphy
 *
 */
public class Setting {
	
	private String name ;
	
	private Map<String, String> options ;
	
	public Setting(@JsonProperty(value="options") Map<String,String> options, 
			@JsonProperty(value="name") String name)
	{
		this.options = options ;
		this.name = name ;
	}

	public Map<String, String> getOptions()
	{
		return options ;
	}
	
	public String getName()
	{
		return name ;
	}
	
	public void setOptions(Map<String, String> options)
	{
		this.options = options ;
	}
	
	public void setName(String name) 
	{
		this.name = name ;
	}

}
