package com.openet.odp.spring.request;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * POJ to represent the parameters for a request to launch an instance
 * @author eoin murphy
 *
 */
public class DemoRequest {

	private final String username ;
	private final String prefered_vim ;
	
	/**
	 * 
	 * @param username
	 * @param prefered_vim
	 */
	public DemoRequest(@JsonProperty("username") String username,
						@JsonProperty("prefered_vim") String prefered_vim)
	{
		this.username 		= username ;
		this.prefered_vim 	= prefered_vim ; 
	}
	
	/**
	 * 
	 * @return username 
	 */
	public String getUsername()
	{
		return username ;
	}
	
	/**
	 * 
	 * @return perfered vim
	 */
	public String getPrefered_vim()
	{
		return prefered_vim ;
	}
	
	/**
	 * checks if the object does not contain any null values after initalisation
	 * @return 
	 */
	public boolean isValid()
	{
//		if(username == null || prefered_vim == null )
//			return false ;
		if(username == null)
			return false ;
		
		return true ;
	}
}
