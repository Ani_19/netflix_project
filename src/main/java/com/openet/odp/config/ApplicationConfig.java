package com.openet.odp.config;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.openet.odp.Application;

public class ApplicationConfig {

	private static final Logger logger = LoggerFactory.getLogger(ApplicationConfig.class); 
	private static Properties config ;
	
	public static Properties getConfig()
	{
		if(config == null)
		{
			config = loadProps("/etc/odp/application.properties") ;
		}
		
		return config ;
	}
	
	/**
	 * loads the properties at the given path
	 * If no properties is found the application will use the default application properties
	 * within the application class path under /resources/application.properties 
	 * @param path
	 * @return
	 */
	private static Properties loadProps(String path)
	{
		Properties prop = new Properties() ;

		try {
			InputStream input = new FileInputStream(path) ;
			prop.load(input) ;
			logger.info("Read properties file: "+path);
		} catch (IOException e) {
			try {
				logger.warn("No such file "+path+" using default properties in class path");
				InputStream input = Application.class.getResourceAsStream("/resources/application.properties") ;
				prop.load(input) ;
			} catch (IOException e1) {
				logger.debug("If i made it here something is very very wrong! Check the jar for the file /resources/application.properties") ;
			}
		}

		return prop ;
	}
}
