package com.openet.odp.config;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

import com.openet.odp.spring.model.MariaDBInfo;
import com.openet.odp.spring.model.OpenstackInfo;
import com.openet.odp.spring.model.WeaverInfo;

@Configuration
@PropertySource(value = "classpath:/resources/application.properties")
public class AppConfig {
    @Autowired
    Environment envrnmnt;
    private static final Logger logger = LoggerFactory.getLogger(ApplicationConfig.class); 
    
    @Bean
    public WeaverInfo getWeaverProperties() {
       logger.info("Getting the weaver information from properties file");
        WeaverInfo wvrInfo = new WeaverInfo();
       wvrInfo.weaverIpAddress = envrnmnt.getProperty("weaver.ipAddress");
       wvrInfo.weaverPort = envrnmnt.getProperty("weaver.port");
    	logger.info("Weaver Hostname: "+wvrInfo.weaverIpAddress+ "Weaver Port: "+wvrInfo.weaverPort);
    	return wvrInfo;
    }
    
    @Bean
    public MariaDBInfo getMariaDBProperties(){
    	logger.info("Getting the MariaDB information from properties file");
    	MariaDBInfo mariaDBInfo = new MariaDBInfo();
    	mariaDBInfo.jdbcUrl = envrnmnt.getProperty("mariadb.jdbcUrl");
    	mariaDBInfo.user = envrnmnt.getProperty("mariadb.user");
    	mariaDBInfo.password = envrnmnt.getProperty("mariadb.password");
    	logger.info("MariaDB jdbcUrl: "+mariaDBInfo.jdbcUrl+ " User: "+mariaDBInfo.user);
    	return  mariaDBInfo;
    }
    
    @Bean
    public OpenstackInfo getOpenstackInfoProperties(){
    	logger.info("Getting the Openstack information from properties file");
    	OpenstackInfo openstackInfo = new OpenstackInfo();
    	openstackInfo.ipAddress = envrnmnt.getProperty("openstack.ipAddress");
    	openstackInfo.port = envrnmnt.getProperty("openstack.port");
    	openstackInfo.password = envrnmnt.getProperty("openstack.password");
    	openstackInfo.user = envrnmnt.getProperty("openstack.user");
    	openstackInfo.tenantId = envrnmnt.getProperty("openstack.tenantId");
    	openstackInfo.apiVersion = envrnmnt.getProperty("openstack.apiVersion");
    	logger.info("Openstack ipAddress: "+openstackInfo.ipAddress+ " Port: "+openstackInfo.port+" ApiVersion: "+openstackInfo.apiVersion);
    	return  openstackInfo;	
    }
}