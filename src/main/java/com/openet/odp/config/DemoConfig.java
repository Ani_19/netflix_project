package com.openet.odp.config;

import java.io.File;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.openet.odp.spring.model.SettingsList;

public class DemoConfig {

	private static final Logger logger = LoggerFactory.getLogger(DemoConfig.class);
	
	private static SettingsList demoConfig ;
	
	public static SettingsList getDemoConfig(String filePath)
	{
		if(demoConfig == null)
		{
			try{
				ObjectMapper objectMapper = new ObjectMapper() ;
				demoConfig = objectMapper.readValue(
						new File(filePath),
						SettingsList.class) ;
				logger.info("Read json demo confiuration file "+filePath);
			}catch(Exception e)
			{
				logger.error("Error reading demoConfiguration json file", e) ;
			}
		}
		
		return demoConfig ;
	}
	
	public static void updateConfig(SettingsList config)
	{
		logger.info("Updating demo configuration with values in object",config);
		demoConfig = config ;
	}
	
	public static void writeConfig(String path)
	{
		logger.info("writing current configuration to file "+path);
		
        try {
        	ObjectMapper objectMapper = new ObjectMapper();
        	objectMapper.configure(SerializationFeature.INDENT_OUTPUT, true);
			objectMapper.writeValue(new File(path), demoConfig);
			
		} catch (JsonGenerationException e) {
			logger.error("Experienced JsonGeneration exception", e);
		} catch (JsonMappingException e) {
			logger.error("Experienced JsonMapping exception", e);
		} catch (IOException e) {
			logger.error("Experienced IO exception", e);
		}
	}
}
