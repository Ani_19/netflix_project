package com.openet.odp.service;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.Date;

import javax.net.ssl.HttpsURLConnection;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.EntityBuilder;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import com.openet.odp.config.AppConfig;
import com.openet.odp.config.OpenstackConnection;
import com.openet.odp.spring.model.MariaDBInfo;
import com.openet.odp.spring.model.OpenstackInfo;

public class OpenstackService {
	
	ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);	   
	OpenstackInfo openstackInfo = context.getBean(OpenstackInfo.class);
	private  final String ipAddress = openstackInfo.getIpAddress();
	private  final String port = openstackInfo.getPort();
	private  final String password = openstackInfo.getPassword();
	private final String tenantId = openstackInfo.getTenantId();
	private final String user = openstackInfo.getUser();
	private final String apiVersion = openstackInfo.getApiVersion();
	
	private final Logger logger = LoggerFactory.getLogger(OpenstackService.class);
	private final int AUTH_SUCCESS_CODE = 200;
	private final int RESUME_SUCCESS_CODE = 202;
	private final String AT_CREATION = "AT_CREATION";
	private final String AT_RELAUNCH = "AT_RELAUNCH";
	private HttpURLConnection connection = null;
///	private Date expireAuthToken = null;
	private String expireAuthToken = null;
	private String authToken = null;
	private String computeURL = null;
	private String serverURL = null;
	private String consoleURL = null;
	private String serverUUID = null;
	
	
	public String getServerUUID(){
		return serverUUID;
	}
	
	
	
	public String configureOpenstack(String opnstackFlowStatus, String instance_id, String demo_id, String serverUUID){
		if(opnstackFlowStatus.equalsIgnoreCase(AT_CREATION)){
				getOpenstackAuthToken();
				if(authToken != null){
					getCreatedServer(demo_id);
					if(serverURL != null){
						getServerConsole();
						return consoleURL;
					}
				}
		}
		
		if(opnstackFlowStatus.equalsIgnoreCase(AT_RELAUNCH)){
			logger.info("Relaunching the server");
			getOpenstackAuthToken();
			if(authToken != null){
				String serverStatus = getServerStatus(serverUUID);
				String activeServerStatus = "ACTIVE";
				String suspServerStatus = "SUSPENDED";
				if(serverStatus.equals(activeServerStatus)){
					DatabaseService dbService = new DatabaseService();
					dbService.updateServerStatusToRelaunch(serverUUID);
					buildActionServerURL(serverUUID);
					getServerConsole();
					return consoleURL;
				}else if(serverStatus.equals(suspServerStatus)){
					int responseCode = resumeSuspendedServer(serverUUID);
					if(responseCode == RESUME_SUCCESS_CODE){
						getServerConsole();
						return consoleURL;
					}
					
				}else{
					return null;
				}
			}

		}
				
		return null;
	}
	
	public void deleteServer(String serverUUID){
		getOpenstackAuthToken();
		
		URL url = null;
		String stUrl = "{computeURL}/servers/{server_id}";
		stUrl = stUrl.replace("{server_id}", serverUUID);
		stUrl = stUrl.replace("{computeURL}", computeURL);
		try {
			logger.info("Delete server url: "+ stUrl);
			url = new URL(stUrl);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		
		try {
			connection = (HttpsURLConnection)url.openConnection();
		} catch (IOException e) {
			logger.info("Inside IO EXception while opening connection");
			e.printStackTrace();
		}

		try {
			connection.setRequestMethod("DELETE");
		} catch (ProtocolException e) {
			e.printStackTrace();
		}
		
		connection.setRequestProperty("Content-Type", "application/json");
		connection.setRequestProperty("X-Auth-Token", authToken);

		connection.setDoOutput(true);
		
		int returnCode = 0;
		try {
			returnCode = connection.getResponseCode();
		} catch (IOException e) {
			e.printStackTrace();
		}
		logger.info("Response code for delete server: "+ returnCode);
	}
	
	private int resumeSuspendedServer(String serverUUID){
		URL url = buildActionServerURL(serverUUID);
		
		try {
			connection = (HttpsURLConnection)url.openConnection();
		} catch (IOException e) {
			logger.info("Inside IO EXception while opening connection");
			e.printStackTrace();
		}

		try {
			connection.setRequestMethod("POST");
		} catch (ProtocolException e) {
			e.printStackTrace();
		}
		
		connection.setRequestProperty("Content-Type", "application/json");
		connection.setRequestProperty("X-Auth-Token", authToken);

		String bodyParam =  "{\"resume\": null}";

		connection.setDoOutput(true);
		DataOutputStream dos;
		try {
			dos = new DataOutputStream(connection.getOutputStream());
			dos.writeBytes(bodyParam);
			dos.flush();
			dos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		int returnCode = 0;
		try {
			returnCode = connection.getResponseCode();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return returnCode;
	}
	
	public String getServerStatus(String serverUUID){
		getOpenstackAuthToken();
		String serverStatus = null;
		if(authToken != null){
			URL url = buildGetServerStatusURL(serverUUID);
			
			try {
				connection = (HttpsURLConnection)url.openConnection();
			} catch (IOException e) {
				logger.info("Inside IO EXception while opening connection");
				e.printStackTrace();
			}
			
			((HttpsURLConnection)connection).setHostnameVerifier(new OpenstackConnection.OpenstackConnectionInner());

			connection.setRequestProperty("Content-Type", "application/json");
			connection.setRequestProperty("X-Auth-Token", authToken);
			try {
				connection.setRequestMethod("GET");
			} catch (ProtocolException e) {
				logger.info("Inside Protocol Exception");
				e.printStackTrace();
			}
			connection.setDoOutput(true);
			
			String servResponse = getAuthResponse();
			if(servResponse != null){
				serverStatus = extractServerStatus(servResponse);
			} 
		}
		return serverStatus;
	}
	
	public int suspendActiveServer(String serverUUID){
		URL url = buildActionServerURL(serverUUID);
		
		try {
			connection = (HttpsURLConnection)url.openConnection();
		} catch (IOException e) {
			logger.info("Inside IO EXception while opening connection");
			e.printStackTrace();
		}

		try {
			connection.setRequestMethod("POST");
		} catch (ProtocolException e) {
			e.printStackTrace();
		}
		
		connection.setRequestProperty("Content-Type", "application/json");
		connection.setRequestProperty("X-Auth-Token", authToken);

		String bodyParam =  "{\"suspend\": null}";

		connection.setDoOutput(true);
		DataOutputStream dos;
		try {
			dos = new DataOutputStream(connection.getOutputStream());
			dos.writeBytes(bodyParam);
			dos.flush();
			dos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		int returnCode = 0;
		try {
			returnCode = connection.getResponseCode();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return returnCode;
	}
	
	private URL buildActionServerURL(String serverUUID){
		logger.info("Building Openstack Suspend Server URL");
		String servURL = computeURL+"/servers/{server_id}";
		servURL = servURL.replace("{server_id}", serverUUID);
		serverURL = servURL;
		servURL = servURL+"/action";
		URL url = null;
		try {
			 url = new URL(servURL);
		} catch (MalformedURLException e) {
			logger.info("Malformed exception occured while building openstack server console url");
			e.printStackTrace();
		}
		logger.info("Built Suspend Server by UUID for openstack: "+servURL);
		return url;	
	}
	
	
	private String extractServerStatus(String serverResponse){
		logger.info("extracting server status");
		String serverStatus = null;
		JSONObject jsonObject;
		try {
			jsonObject = new JSONObject(serverResponse);
			JSONObject server = jsonObject.getJSONObject("server");
			serverStatus = server.getString("status");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		logger.info("Server Status: "+serverStatus);
		return serverStatus; 
	}
	
	private URL buildGetServerStatusURL(String serverUUID){
		logger.info("Building Openstack Server Status URL");
		String servURL = computeURL+"/servers/{server_id}";
		servURL = servURL.replace("{server_id}", serverUUID);
		URL url = null;
		try {
			 url = new URL(servURL);
		} catch (MalformedURLException e) {
			logger.info("Malformed exception occured while building openstack server console url");
			e.printStackTrace();
		}
		logger.info("Built Get Server by UUID for openstack: "+servURL);
		return url;	
	}
	
	
	private URL buildServerConsoleURL(){
		logger.info("Building Openstack Server Console URL");
		String servConURL = serverURL+"/action";
		URL url = null;
		try {
			 url = new URL(servConURL);
		} catch (MalformedURLException e) {
			logger.info("Malformed exception occured while building openstack server console url");
			e.printStackTrace();
		}
		logger.info("Built Server Console Url by tag for openstack: "+servConURL);
		return url;		
	}
	
	private void getServerConsole(){
		URL url = buildServerConsoleURL();
		
		try {
			connection = (HttpsURLConnection)url.openConnection();
		} catch (IOException e) {
			logger.info("Inside IO EXception while opening connection");
			e.printStackTrace();
		}

		try {
			connection.setRequestMethod("POST");
		} catch (ProtocolException e) {
			e.printStackTrace();
		}
		
		connection.setRequestProperty("Content-Type", "application/json");
		connection.setRequestProperty("X-Auth-Token", authToken);

		String bodyParam =  "{\"os-getVNCConsole\": {\"type\": \"novnc\"}}";

		connection.setDoOutput(true);
		DataOutputStream dos;
		try {
			dos = new DataOutputStream(connection.getOutputStream());
			dos.writeBytes(bodyParam);
			dos.flush();
			dos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		String servConsoleResponse = getAuthResponse();	
		if(servConsoleResponse != null){
			extractConsoleURL(servConsoleResponse);
		} 
		
	}
	
	private void extractConsoleURL(String servCOnResp){
		logger.info("extracting console server url");
		JSONObject jsonObject;
		try {
			jsonObject = new JSONObject(servCOnResp);
			JSONObject console = jsonObject.getJSONObject("console");
			consoleURL = console.getString("url");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		logger.info("Server Console URL: "+consoleURL);
	}
	
	

	

	

	
	private void getCreatedServer(String demo_id){
		URL url = buildServerURL(demo_id);
		
		try {
			connection = (HttpsURLConnection)url.openConnection();
		} catch (IOException e) {
			logger.info("Inside IO EXception while opening connection");
			e.printStackTrace();
		}
		
		((HttpsURLConnection)connection).setHostnameVerifier(new OpenstackConnection.OpenstackConnectionInner());

		connection.setRequestProperty("Content-Type", "application/json");
		connection.setRequestProperty("X-Auth-Token", authToken);
		try {
			connection.setRequestMethod("GET");
		} catch (ProtocolException e) {
			logger.info("Inside Protocol Exception");
			e.printStackTrace();
		}
		connection.setDoOutput(true);
		
		String servResponse = getAuthResponse();
		if(servResponse != null){
			extractCreatedServerURL(servResponse);
		} 
	}
	
	private void extractCreatedServerURL(String servResponse){
		logger.info("Extracting Created Server's URL");
		JSONObject jsonObject;
		try {
			jsonObject = new JSONObject(servResponse);
			JSONArray servers = jsonObject.getJSONArray("servers");
			JSONObject firstServer = servers.getJSONObject(0);
			logger.info("Getting URL for server name: "+firstServer.getString("name"));
			String serverId = firstServer.getString("id");
			serverUUID = serverId;
			serverURL = computeURL+"/servers/"+serverUUID;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		logger.info("Server URL: "+serverURL);
	}		
	
	private URL buildServerURL(String demo_id){
		logger.info("Building Openstack Server URL");
		String filterOne = "?name=^"+demo_id;
		String filterTwo = "&limit=4";
		String servURL = computeURL+"/servers";
		servURL += filterOne;
		servURL += filterTwo;
		URL url = null;
		try {
			 url = new URL(servURL);
		} catch (MalformedURLException e) {
			logger.info("Malformed exception occured while building openstack server url");
			e.printStackTrace();
		}
		logger.info("Built server Url for openstack: "+servURL);
		return url;		
	}
	
	private void getOpenstackAuthToken(){
		URL url = buildAuthUrl();
		String reqAuthBody = buildAuthRequestBody();
		
		try {
			connection = (HttpsURLConnection)url.openConnection();
		} catch (IOException e) {
			logger.info("Inside IO EXception while opening connection");
			e.printStackTrace();
		}
		
		((HttpsURLConnection)connection).setHostnameVerifier(new OpenstackConnection.OpenstackConnectionInner());

		connection.setRequestProperty("Content-Type", "application/json");
		try {
			connection.setRequestMethod("POST");
		} catch (ProtocolException e) {
			logger.info("Inside Protocol Exception");
			e.printStackTrace();
		}
		connection.setDoOutput(true);
		OutputStream outputStream = null;
		try {
				outputStream = connection.getOutputStream();
		} catch (IOException e) {
			logger.info("Inside stream IOException");
			e.printStackTrace();
		}
		
		try {
			outputStream.write(reqAuthBody.getBytes());
			outputStream.flush();
		} catch (IOException e) {
			logger.info("Inside body IOException");
			e.printStackTrace();
		}
		
		String authResponse = getAuthResponse();
		 if(authResponse != null){
			 extractAuthDetailsFromResponse(authResponse);
		 }		
	}

	private void extractAuthDetailsFromResponse(String authResponse){
			extractAuthExpiryDate(authResponse);
			extractAuthToken(authResponse);
			extractComputeURL(authResponse);
	}
	
	private void extractAuthToken(String authResp){
		logger.info("Extracting Openstack Authentication Token");
		JSONObject jsonObject;
		try {
			jsonObject = new JSONObject(authResp);
			JSONObject access = jsonObject.getJSONObject("access");
			JSONObject token = access.getJSONObject("token");
			authToken = token.getString("id");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		logger.info("Authentication Token: "+authToken);
	}
	
	private void extractAuthExpiryDate(String authResp){
		logger.info("Extracting Openstack Authentication Expiry Date");
		JSONObject jsonObject;
		try {
			jsonObject = new JSONObject(authResp);
			JSONObject access = jsonObject.getJSONObject("access");
			JSONObject token = access.getJSONObject("token");
			expireAuthToken = token.getString("expires");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		logger.info("Expiry Date: "+expireAuthToken);
	}
	
	private void extractComputeURL(String authResp){
		logger.info("Extracting Openstack Compute(Nova) URL");
		JSONObject jsonObject;
		try {
			jsonObject = new JSONObject(authResp);
			JSONObject access = jsonObject.getJSONObject("access");
			JSONArray srvcCatalog = access.getJSONArray("serviceCatalog");
			
	 		for(int i=0; i < srvcCatalog.length(); i++){
	 			JSONObject tempObject = srvcCatalog.getJSONObject(i);
	 			String tempType = tempObject.getString("type");
	 			String tempName = tempObject.getString("name");
	 			if((tempType.equalsIgnoreCase("compute")) && (tempName.equalsIgnoreCase("nova"))){
	 				JSONArray endpoints = tempObject.getJSONArray("endpoints");
	 				JSONObject jsObject = endpoints.getJSONObject(0);
	 				computeURL = jsObject.getString("publicURL");
	 				break;
	 			}else{
	 				continue;
	 			}
	 		} 
		} catch (JSONException e) {
			e.printStackTrace();
		}
		logger.info("Compute(Nova) URL: "+computeURL);		
	}
	
	private String getAuthResponse(){
		logger.info("Getting response from openstack authentication request");

		StringBuilder stringBuilder = new StringBuilder();
		try {
			if(connection.getResponseCode() == AUTH_SUCCESS_CODE){
				logger.info("Openstack Auth Response Code: "+connection.getResponseCode());
				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
				String line;
				while((line = bufferedReader.readLine()) != null){
					stringBuilder.append(line + "\n");			
				}
				bufferedReader.close();			
			}else{
				logger.info("Openstack Response Code: "+connection.getResponseCode());
				return null;
			}
		} catch (IOException e){
			e.printStackTrace();
		}	
		logger.info("Openstack Auth Response Message: "+stringBuilder.toString());
		return stringBuilder.toString();
	}
	

	
	private URL buildAuthUrl(){
		logger.info("Building url for openstack authentication request");
		String authUrl = "https://{ipAddress}:{port}/{api_version}/tokens";
		authUrl = authUrl.replace("{ipAddress}", ipAddress);
		authUrl = authUrl.replace("{port}", port);
		authUrl = authUrl.replace("{api_version}", apiVersion);
		URL url = null;
		try {
			 url = new URL(authUrl);
		} catch (MalformedURLException e) {
			logger.info("Malformed exception occured while building openstack authentication url");
			e.printStackTrace();
		}
		logger.info("Built auth Url for openstack: "+authUrl);
		return url;
	}
	
	private String buildAuthRequestBody(){
		logger.info("Building body for openstack authentication request");
		String reqAuthBody = "{\"auth\": {\"passwordCredentials\": {\"username\": \"{username}\", \"password\": \"{password}\"}, \"tenantId\": \"{tenant_id}\"}}";
		reqAuthBody = reqAuthBody.replace("{username}", user);
		reqAuthBody = reqAuthBody.replace("{password}", password);
		reqAuthBody = reqAuthBody.replace("{tenant_id}", tenantId);
		logger.info("Built request: "+reqAuthBody);
		return reqAuthBody;
	}
	




	
	public void getConsoleTwo() throws IOException{
		String url = "https://public.fuel.local:8774/v2/b27eaf3b9b774d5c9d8ca81f28d472f1/servers/"+"d6dace32-15f3-40ae-aab1-2294f2f4c246"+"/action";
		URL obj = new URL(url);
		HttpsURLConnection con = (HttpsURLConnection)obj.openConnection();

		//add reuqest header
		con.setRequestMethod("POST");
		con.setRequestProperty("Content-Type", "application/json");
		con.setRequestProperty("X-Auth-Token", "14f786aa4a974b38af46e78a150cbe8a");
	//	con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
	//	con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
//String urlParameters =  "{\"os-getRDPConsole\": {\"type\": \"rdp-html5\"}}";
//String urlParameters =  "{\"os-getSPICEConsole\": {\"type\": \"spice-html5\"}}";
String urlParameters =  "{\"os-getVNCConsole\": {\"type\": \"novnc\"}}";
//String urlParameters =  "{\"os-getSerialConsole\": {\"type\": \"serial\"}}";
		// Send post request
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();

		int responseCode = con.getResponseCode();
		System.out.println("\nSending 'POST' request to URL : " + url);
		System.out.println("Post parameters : " + urlParameters);
		System.out.println("Response Code : " + responseCode);

		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		//print result
		System.out.println(response.toString());
	}

	
	
	
}
