package com.openet.odp.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.inject.Inject;

import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.openet.odp.spring.response.LoginResponse;

@Service
public class UserService {
	
	@Inject
	private Environment env ;
	
	public LoginResponse login(String username, String password) throws SQLException
	{
		LoginResponse loginResponse = null ;
		Connection connection = DriverManager.getConnection(
				env.getProperty("mariadb.jdbcUrl"), 
				env.getProperty("mariadb.user"), 
				env.getProperty("mariadb.password")) ;
		
		Statement stmt = connection.createStatement() ;
		ResultSet result = stmt.executeQuery("call validate_user('"+username+"','"+password+"')") ;
		if(result.first() == true)
		{
			String msg = result.getString("msg") ;
			if(msg.equals("success"))
			{
				String role = result.getString("role") ;
				loginResponse = new LoginResponse(username,true,msg,role) ;
			}
			else
			{
				loginResponse = new LoginResponse(username, false, msg,null) ;
			}
		}
		
		return loginResponse ;
	}
}
