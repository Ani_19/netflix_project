package com.openet.odp.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DeleteInstanceService implements Runnable {
private String UUID;
private final Logger logger = LoggerFactory.getLogger(DeleteInstanceService.class);


	public void run() {
		logger.info("Starting Deleting instance for serverID: "+UUID);
		deleteInstanceFromFIFO();
		deleteOpenstackServer();
		deleteInstanceFromDB();
		logger.info("Deleting instance completed successfully for serverID: "+UUID);
	}
	
	public DeleteInstanceService(String serverUUID){
		this.UUID = serverUUID;
	}
	
	private void deleteInstanceFromFIFO(){
		DatabaseService dbService = new DatabaseService();
		dbService.deleteUsedUUIDEntry(UUID, false);		
	}
	
	private void deleteOpenstackServer(){
		logger.info("$$$$$$$$$$$$$$$$$$$$$$$$");
		OpenstackService openstackService = new OpenstackService();
		openstackService.deleteServer(UUID);
		logger.info("$-$_$_$_$_$_$_$_$_$_$_$_$_$_");
	}
	
	private void deleteInstanceFromDB(){
		DatabaseService dbService = new DatabaseService();
		dbService.deleteEntryFromInstance(UUID);
	}

}
