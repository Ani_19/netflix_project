package com.openet.odp.service;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.EntityBuilder;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Service;

import com.openet.odp.config.AppConfig;
import com.openet.odp.spring.model.WeaverInfo;

@Service
public class WeaverService {
	
	private final HttpClient httpClient = HttpClientBuilder.create().build();
	private final Logger logger = LoggerFactory.getLogger(WeaverService.class);
	private final String WORKFLOW_INSTANTIATE_SUC_CODE = "201";
	private final Long INSTANTIATE_WAITING_TIME = 600000L;
	private final int MAX_CHECK_WRKFLOW_REQ = 10;
	private final Long TIME_BTW_REQ = 60000L;
	private final String RUN_STATUS = "RUNNING";
	private final String COMP_STATUS = "COMPLETED";
	private final String FAIL_STATUS = "FAILED";
	
	
	ApplicationContext ctx = new AnnotationConfigApplicationContext(AppConfig.class);	   
	WeaverInfo wvrInfo = ctx.getBean(WeaverInfo.class);
	private String weaverIpAddr = wvrInfo.getWeaverIpAddress();
	private int weaverPort = Integer.parseInt(wvrInfo.getWeaverPort());
	private final HttpHost weaverHost =  new HttpHost(weaverIpAddr, weaverPort);
	
	private DatabaseService dbService = new DatabaseService();
	
	public String launchWeaverVNF(String demo_id, String username) throws SQLException
	{
		String instance_id = DateTime.getDBDateTime();
		String vnf_id,tenant_id,vnf_template_id, vnfd_id ;	
				
		ResultSet resultSet = dbService.getWeaverDetailsForLaunch(demo_id);
		
		if(resultSet.first() == true)
		{
			vnf_id = resultSet.getString("vnf_id");
			tenant_id = resultSet.getString("tenant_id");
			vnf_template_id = resultSet.getString("vnf_template_id") ;
			vnfd_id = resultSet.getString("vnfd_id") ;
		}
		else
		{
			logger.info(DateTime.getCurrentDateTime().toString()+ ":::Returning instance_id: null, as no details found for requested demo.");
			instance_id = null;
			return instance_id ; 
		}

		String reqEndpoint = buildIntansiateVNFRequest(vnf_id, tenant_id, instance_id);
		String reqBody = buildInstantiateVNFReqBody(vnf_template_id, vnfd_id);
		
		HttpResponse instantiateResp = null;
		if((instantiateResp = postVNFInstantiateRequest(reqEndpoint, reqBody)) != null){
			HashMap<String, String> map = getDataFromInstantiateResponse(instantiateResp);
			String respStatus = map.get("STATUS");
			String wrkflowInstanceId = map.get("WORKFLOW_INSTANCE_ID");
			if(respStatus.equalsIgnoreCase(WORKFLOW_INSTANTIATE_SUC_CODE)){
				String wrkflowStatus = checkWorkflowStatus(reqEndpoint, wrkflowInstanceId);
				logger.info("Final Workflow Status: "+wrkflowStatus);
				if(wrkflowStatus.equalsIgnoreCase(COMP_STATUS)){
					return instance_id;
				}else{
					instance_id = null;
					return instance_id;	
				}
				
			}else{
				instance_id = null;
				return instance_id;			
			}
		}else{
			instance_id = null;
			return instance_id;
		}
	}
	
	private String checkWorkflowStatus(String reqEndpoint, String workflowInstanceId){
		String wfInstanceStatus = null;
		long startTime = System.currentTimeMillis();
		
		loop1:
		while((System.currentTimeMillis()-startTime)<INSTANTIATE_WAITING_TIME)
		{
			for(int i = 0; i< MAX_CHECK_WRKFLOW_REQ; i++) {
			    try {
			       logger.info("Waiting to send GET request to check workflow status");
			        Thread.sleep(TIME_BTW_REQ);
			        logger.info("Sending the GET request.");
			        wfInstanceStatus = getWorkflowStatus(reqEndpoint, workflowInstanceId);
			        if((wfInstanceStatus.equalsIgnoreCase(COMP_STATUS)) || ((wfInstanceStatus.equalsIgnoreCase(FAIL_STATUS)))){
			        	break loop1;
			        }else if(wfInstanceStatus.equalsIgnoreCase(RUN_STATUS)){
			        	continue;
			        }else{
			        	break loop1;
			        }
			    } catch(InterruptedException ie) {
			    	ie.printStackTrace();
			    }
			       
			}
		}
		
		return wfInstanceStatus;
	}
	
	private String getWorkflowStatus(String reqEndpoint, String workflowInstanceId){
		String workflowEndpoint = reqEndpoint + "/" + workflowInstanceId;
		logger.info("GET request: "+workflowEndpoint);
		String workflowStatus = null;
		HttpResponse workflowStatusResponse = null;
		HttpGet get = new HttpGet(workflowEndpoint);
			try {
				workflowStatusResponse  = httpClient.execute(weaverHost, get);
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

			workflowStatus = getWorkflowStatusFromResponse(workflowStatusResponse);
			logger.info("Workflow Status: "+workflowStatus);
		return workflowStatus;		
	}
	
	private String getWorkflowStatusFromResponse(HttpResponse workflowStatusResponse){
		String jsonString = null;
		try {
			jsonString = EntityUtils.toString(workflowStatusResponse.getEntity());
		} catch (ParseException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		String workflowStatus = null;
		JSONObject jsonObj;
		try {
			jsonObj = new JSONObject(jsonString);
			 JSONObject data = jsonObj.getJSONObject("data");
			 workflowStatus = data.getString("workflow_instance_status"); 
		} catch (JSONException e) {
			e.printStackTrace();
		}
		 return workflowStatus;
	}
	
	private HashMap<String, String> getDataFromInstantiateResponse(HttpResponse instantiateResp){
		HashMap<String, String> map = new HashMap<String, String>();
		
		String workflowInstanceId = null;
		String workflowStatus = null;

		try {
			String jsonString = EntityUtils.toString(instantiateResp.getEntity());
			JSONObject jsonObj = new JSONObject(jsonString);
			logger.info(DateTime.getCurrentDateTime().toString()+ ":::Printing the JSON result from instantiate request:: "+jsonObj);
			JSONObject data = jsonObj.getJSONObject("data");
			workflowInstanceId = data.getString("workflow_instance_id");			
			workflowStatus = jsonObj.getString("status");			
			logger.info(DateTime.getCurrentDateTime().toString()+ ":::workflowStatus: "+workflowStatus+" workflow_instance_id: "+workflowInstanceId);
			map.put("STATUS", workflowStatus);
			map.put("WORKFLOW_INSTANCE_ID", workflowInstanceId);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return map;
	}
	
	
	private HttpResponse postVNFInstantiateRequest(String endpoint, String requestBody){
		HttpPost post = new HttpPost(endpoint);	
		EntityBuilder body = EntityBuilder.create();
		body.setContentType(ContentType.APPLICATION_JSON).setText(requestBody);
		HttpEntity request = body.build(); 
		post.setEntity(request);
		HttpResponse instantiateResponse = null;
		
		try {	
			logger.info(DateTime.getCurrentDateTime().toString()+ ":::Executing POST request to weaver:::weaverIpAddr: "+weaverIpAddr+" weaverPort: "+weaverPort);
			instantiateResponse = httpClient.execute(weaverHost, post) ;
		} catch (ClientProtocolException cpException) {
			logger.info(DateTime.getCurrentDateTime().toString()+ ":::ClientProtocolException occured while posting request to weaver.");
			cpException.printStackTrace();
		} catch (IOException ioException) {
			logger.info(DateTime.getCurrentDateTime().toString()+ ":::IOException occured while posting request to weaver.");
			ioException.printStackTrace();
		}
		
		return instantiateResponse;
	}
	
	private String buildIntansiateVNFRequest(String vnf_id, String tenant_id, String instance_id){
		logger.info(DateTime.getCurrentDateTime().toString()+ ":::Building request endpoint for instantiating vnf");
		
		String requestEndpoint = "/api/v1/tenants/{tenant_id}/vnfs/{vnf_id}/vnf_instances/{vnf_instance_id}/workflow_instances" ;
		requestEndpoint = requestEndpoint.replace("{tenant_id}", tenant_id);
		requestEndpoint = requestEndpoint.replace("{vnf_id}", vnf_id);
		requestEndpoint = requestEndpoint.replace("{vnf_instance_id}", instance_id);
		
		logger.info(DateTime.getCurrentDateTime().toString()+ ":::Built request endpoint: "+requestEndpoint);
		return requestEndpoint;
	}
	
	private String buildInstantiateVNFReqBody(String vnf_template_id, String vnfd_id){
		logger.info(DateTime.getCurrentDateTime().toString()+ ":::Building request body for instantiating vnf");
		String instansiateRequestBody = "{\"workflow_type\": \"instantiate_vnf\", \"vnfd\":{\"vnf_template_id\":\"[tempate_val]\",\"vnfd_id\":\"[vnfd_id_val]\"}}" ;
		
		instansiateRequestBody = instansiateRequestBody.replace("[tempate_val]", vnf_template_id) ;
		instansiateRequestBody = instansiateRequestBody.replace("[vnfd_id_val]", vnfd_id) ;
		
		logger.info(DateTime.getCurrentDateTime().toString()+"Built weaver workflow request body: "+instansiateRequestBody);
		return 	instansiateRequestBody;	
	}

	
}
