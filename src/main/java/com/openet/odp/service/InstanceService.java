package com.openet.odp.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.net.ssl.HttpsURLConnection;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.EntityBuilder;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
//import org.omg.CORBA.portable.OutputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.ec2.AmazonEC2Client;
import com.amazonaws.services.ec2.model.CreateTagsRequest;
import com.amazonaws.services.ec2.model.Reservation;
import com.amazonaws.services.ec2.model.RunInstancesRequest;
import com.amazonaws.services.ec2.model.RunInstancesResult;
import com.amazonaws.services.ec2.model.Tag;
import com.amazonaws.services.ec2.model.TerminateInstancesRequest;
import com.amazonaws.services.ec2.model.TerminateInstancesResult;
import com.openet.odp.config.AppConfig;
import com.openet.odp.config.OpenstackConnection;
import com.openet.odp.spring.model.Instance ;
import com.openet.odp.spring.model.WeaverInfo; 


@Service
public class InstanceService {
	
	@Inject
	private Environment env ;
//	private final HttpClient httpClient = HttpClientBuilder.create().build();
	private final Logger logger = LoggerFactory.getLogger(InstanceService.class);
	private final long SERVER_RUNNING_TIME = 5;
//	private Date SERVER_SHUTDOWN_TIME;
	private DatabaseService dbService = new DatabaseService();
	public List<Instance> getAllInstances() throws SQLException
	{
		logger.debug("Entered service method for getAllInstances REST endpoint");
		List<Instance> instances = new ArrayList<Instance>() ;
		
		Connection connection = DriverManager.getConnection(
				env.getProperty("mariadb.jdbcUrl"),
				env.getProperty("mariadb.user"),
				env.getProperty("mariadb.password")) ;
		
		Statement stmt = connection.createStatement() ;
		ResultSet result = stmt.executeQuery(
				"SELECT instance_id, demo_id, snapshot_id, created_date, username, vim FROM instances") ;
		
		while(result.next() != false)
		{
			instances.add(
					new Instance(
							result.getString("instance_id"),
							result.getString("demo_id"),
							result.getString("snapshot_id"),
							result.getString("created_date"),
							result.getString("username"),
							result.getString("vim")
							) 
					);
		}
		
		//Close of our connections to the DB
		stmt.close();
		result.close();
		connection.close();
		
		return instances ;
	}
	
	public List<Instance> getUsersInstances(String username) throws SQLException
	{
		logger.debug("Entered service method for getDemoInstances REST end point");
		List<Instance> instances = new ArrayList<Instance>() ;
		
		Connection connection = DriverManager.getConnection(
				env.getProperty("mariadb.jdbcUrl"),
				env.getProperty("mariadb.user"),
				env.getProperty("mariadb.password")) ;
		
		Statement stmt = connection.createStatement() ;
		ResultSet result = stmt.executeQuery(
				"SELECT instance_id, demo_id, snapshot_id, created_date, username, vim FROM instances WHERE username = '"+username+"'" ) ;
		
		while(result.next() != false)
		{
			instances.add(
					new Instance(
							result.getString("instance_id"),
							result.getString("demo_id"),
							result.getString("snapshot_id"),
							result.getString("created_date"),
							result.getString("username"),
							result.getString("vim")
							) 
					);
		}
		
		//Close of our connections to the DB
		stmt.close();
		result.close();
		connection.close();
		
		return instances ;
	}
	
	
	public List<Instance> getDemoInstances(String demo_id) throws SQLException
	{
		logger.debug("Entered service method for getDemoInstances REST end point") ;
		List<Instance> instances = new ArrayList<Instance>() ;
		
		Connection connection = DriverManager.getConnection(
				env.getProperty("mariadb.jdbcUrl"),
				env.getProperty("mariadb.user"),
				env.getProperty("mariadb.password")) ;
		
		Statement stmt = connection.createStatement() ;
		ResultSet result = stmt.executeQuery(
				"SELECT instance_id, demo_id, snapshot_id, created_date, username, vim FROM instances WHERE demo_id = '"+demo_id+"'" ) ;
		
		while(result.next() != false)
		{
			instances.add(
					new Instance(
							result.getString("instance_id"),
							result.getString("demo_id"),
							result.getString("snapshot_id"),
							result.getString("created_date"),
							result.getString("username"),
							result.getString("vim")
							) 
					);
		}
		
		//Close of our connections to the DB
		stmt.close();
		result.close();
		connection.close();
		
		return instances ;
	}
	
	public List<Instance> getUserDemoInstances(String demo_id, String username) throws SQLException
	{
		List<Instance> instances = new ArrayList<Instance>() ;
		
		Connection connection = DriverManager.getConnection(
				env.getProperty("mariadb.jdbcUrl"),
				env.getProperty("mariadb.user"),
				env.getProperty("mariadb.password")) ;
		
		Statement stmt = connection.createStatement() ;
		ResultSet result = stmt.executeQuery(
				"SELECT instance_id, demo_id, snapshot_id, created_date, username, vim FROM instances WHERE demo_id = '"+demo_id+"' AND username ='"+username+"'" ) ;
		
		while(result.next() != false)
		{
			instances.add(
					new Instance(
							result.getString("instance_id"),
							result.getString("demo_id"),
							result.getString("snapshot_id"),
							result.getString("created_date"),
							result.getString("username"),
							result.getString("vim")
							) 
					);
		}
		
		//Close of our connections to the DB
		stmt.close();
		result.close();
		connection.close();
		
		return instances ;
	}
	

	public String launchNewDemo(String demo_id, String username) throws SQLException
	{	

		String preferredVim = "openstack";
		WeaverService wvrSrvc = new WeaverService();
		String instance_id = wvrSrvc.launchWeaverVNF(demo_id, username);
		logger.info("instanceID: "+instance_id);
		if(instance_id == null){
			logger.info("The creation of instance failed on weaver(Openstack)");
			return null;
		}
		if(instance_id != null)
		{
			String opnstackFlowStatus = "AT_CREATION";
			OpenstackService openstackService = new OpenstackService();
			//OpenstackResponse should be the console url
			String openstackResponse = openstackService.configureOpenstack(opnstackFlowStatus, instance_id, demo_id, null);
			
			ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
			executor.schedule(new OpenstackSnapshotService(), SERVER_RUNNING_TIME, TimeUnit.MINUTES);
	
			
			String instanceName = demo_id+"_"+username+"_"+instance_id;
			dbService.saveInstanceCreation(instance_id, username, instanceName, demo_id, preferredVim, openstackService.getServerUUID());
			return  openstackResponse ;
		} 	
		return null;
	}
	
	public String reLaunchNewDemo(String demo_id, String username){
		String UUID = null;
		String consoleURL = null;
		try {
			UUID = getExistingDemoUUID(demo_id, username);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		Runnable runnable = new DeleteInstanceService(UUID);
		new Thread(runnable).start();
		
		try {
			consoleURL = launchNewDemo(demo_id, username);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		if(consoleURL != null){
			return consoleURL;
		}
		
		return null;
	}
	
	public String relaunchDemo(String demo_id, String username){
		//1) get  the demo status: get the UUID where 
		String UUID = null;
	//	String serverStatus = null;
		String opnstackFlowStatus = "AT_RELAUNCH";
		DatabaseService dbService = new DatabaseService();
		OpenstackService openstackService = new OpenstackService();
		try {
			UUID = getExistingDemoUUID(demo_id, username);
		} catch (SQLException e) {
			e.printStackTrace();
		}
				
		String consoleURL = openstackService.configureOpenstack(opnstackFlowStatus, null, demo_id, UUID);
		
		if(consoleURL != null){
			dbService.saveServerCreatedTime(UUID);

			ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
			executor.schedule(new OpenstackSnapshotService(), SERVER_RUNNING_TIME, TimeUnit.MINUTES);
			
			return consoleURL;
		}
		
		return null;
		
	}
	
	private String getExistingDemoUUID(String demo_id, String username) throws SQLException{
		ResultSet result = dbService.getExistingDemoUUIDForUser(demo_id, username); 
		String UUID = null;
		if(result.first() == true){
			UUID = result.getString("UUID");
		}
		return UUID;
	}
	
//	public void setServerSuspendTime(String instance_id){
//		logger.info("Setting server shutdown time");
//		Date date = null;
//		DateFormat dateFormat = DateTime.getDBDateFormat();
//		try {
//			date = dateFormat.parse(instance_id);
//		} catch (java.text.ParseException e) {
//			e.printStackTrace();
//		}
//		Calendar c1 = Calendar.getInstance();
//		c1.setTime(date);
//		c1.add(Calendar.MINUTE, SERVER_RUNNING_TIME);
//		SERVER_SHUTDOWN_TIME = c1.getTime();
//		logger.info("Server shutdown time: "+SERVER_SHUTDOWN_TIME.toString());
//	}
	
	public boolean deleteDemo(String demo_id, String username) throws SQLException
	{
		String instance_id = null ;
		String vim = null ;
		Connection connection = DriverManager.getConnection(
				env.getProperty("mariadb.jdbcUrl"),
				env.getProperty("mariadb.user"),
				env.getProperty("mariadb.password")) ;
		
		Statement stmt = connection.createStatement() ;

		ResultSet result = stmt.executeQuery(
				"SELECT instance_id, vim FROM instances WHERE demo_id = '"+
						demo_id+"' && username = '"+
						username+"'") ;
		
		if(result.first() == true)
		{
			instance_id = result.getString("instance_id") ;
			vim = result.getString("vim") ;	
		}else
		{
			return false ;
		}
		
		if(vim.equals("aws"))
			deleteAWSInstance(instance_id) ;
		else
			deleteWeaverVNF(instance_id) ;
		
		dbService.saveInstanceDeletion(instance_id, username, vim) ;
		
		result.close();
		stmt.close();
		connection.close();
		return true ;
	}
	

	

	
	
	

	
	
	/**
	 * 
	 * @param connection
	 * @param demo_id
	 * @param instanceName
	 * @return String instanceId if successfully created otherwise null
	 * @throws SQLException
	 */
	private String launchAWSInstance(Connection connection,String demo_id, String instanceName) throws SQLException
	{
		logger.info("Launching AWS instance "+instanceName+" for demo: "+demo_id);
		String ami, type, subnet ;
		String instance_id = null ;
		// get the instance details from the db
		Statement stmt = connection.createStatement() ;
		ResultSet result = stmt.executeQuery("SELECT ami, type, subnet_id FROM demo_aws_details WHERE demo_id = '"+demo_id+"'") ;

		if(result.first() == true)
		{
			ami = result.getString("ami") ;
			type = result.getString("type") ;
			subnet = result.getString("subnet_id") ;
			logger.info("AWS instance ami: "+ami+" type: "+type) ;
		}
		// if we got no results close our mariadb connections and return null ;
		else
		{
			result.close();
			stmt.close(); 
			return instance_id ; // this should be a demo not found exception
		}
		
		result.close();
		stmt.close();
		
		instance_id = sendRunAWSInstacneRequest(ami, type, subnet, instanceName) ;
		return instance_id ;
	}
	
	/**
	 * Creates and sends an api requset on aws to launch an instance of the ami passed
	 * @param ami
	 * @return id of the instance created on aws
	 */
	// This is a work around until weaver gets aws support
	private String sendRunAWSInstacneRequest(String ami, String type, String subnet, String instanceName)
	{
		logger.info("Sending Request to aws to launch instance");
		AWSCredentials credentials = new BasicAWSCredentials(
				env.getProperty("aws.security.accesskey"),
				env.getProperty("aws.security.secretaccesskey")) ;
		//create our client with the credentials and set our endpoint
		AmazonEC2Client EC2Client = new AmazonEC2Client(credentials) ;
		EC2Client.setEndpoint(
				env.getProperty("aws.endpoint"));
		
		// build the aws runInstance api request
		RunInstancesRequest runInstancesRequest =
			      new RunInstancesRequest();
		
		runInstancesRequest.withImageId(ami)
	                     .withInstanceType(type) //need to set this in a demo config, or do i as this is just a work around untill aws is added to weaver
	                     .withSubnetId(subnet)
	                     .withMinCount(1)
	                     .withMaxCount(1)
	                     .withKeyName(
	                    		 env.getProperty("aws.security.keypair"))
	                     .withSecurityGroupIds(
	                    		 env.getProperty("aws.security.groupid"));
		
		logger.info(runInstancesRequest.toString());
		
		RunInstancesResult runInstancesResult =
				EC2Client.runInstances(runInstancesRequest);
		
		logger.info(runInstancesResult.toString());
		
		// Get the instance id asscoated with the newly reservced instance on AWS.
		Reservation reservation = runInstancesResult.getReservation() ;
		String awsInstanceId = reservation.getInstances().get(0).getInstanceId() ;
		
		createInstanceNameTag(EC2Client, instanceName, awsInstanceId) ;
		
		return awsInstanceId;
	}
	
	/**
	* creates a tag in aws association the value name with the instanceName
	* passed and attaches it to the instance with the id passed
	*/
	private void createInstanceNameTag(AmazonEC2Client EC2Client, String name, String instanceId)
	{
		logger.info("Creating name tag for instance: "+instanceId);
		
		Tag t = new Tag();
		t.setKey("Name") ;
		t.setValue(name) ;

		CreateTagsRequest ctr = new CreateTagsRequest();
		ctr.withTags(t) ;
		ctr.withResources(instanceId) ;
		
		logger.info(ctr.toString());
		
		EC2Client.createTags(ctr) ;		

	}	
	
	private static void deleteWeaverVNF(String instance_id)
	{
		
	}
	
	private void deleteAWSInstance(String instanceId)
	{
		AWSCredentials credentials = new BasicAWSCredentials(
				env.getProperty("aws.security.accesskey"),
				env.getProperty("aws.security.secretaccesskey")) ;
		//create our client with the credentials and set our endpoint
		AmazonEC2Client EC2Client = new AmazonEC2Client(credentials) ;
		EC2Client.setEndpoint(
				env.getProperty("aws.endpoint"));
		
		TerminateInstancesRequest terminateRequest = 
				new TerminateInstancesRequest() ;
		
		terminateRequest.withInstanceIds(instanceId) ;
		TerminateInstancesResult result = 
				EC2Client.terminateInstances(terminateRequest) ;
		
		logger.info(result.toString());
		
 	}
	


	

}
