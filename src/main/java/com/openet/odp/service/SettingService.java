package com.openet.odp.service;


import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.openet.odp.config.DemoConfig;
import com.openet.odp.spring.model.SettingsList;
import com.openet.odp.spring.model.Setting;

@Service
public class SettingService {

	private static final Logger logger = LoggerFactory.getLogger(SettingService.class);
	
	@Inject
	private Environment env ;
	
	public SettingsList getAllSettings()
	{
		String demoSettingsPath = env.getProperty("config.json.demo") ;
		SettingsList demoConfig = DemoConfig.getDemoConfig(demoSettingsPath) ;
		
		return demoConfig ;
	}
	
	public Setting getSetting(String name)
	{
		String demoSettingsPath = env.getProperty("config.json.demo") ;
		SettingsList demoConfig = DemoConfig.getDemoConfig(demoSettingsPath) ;
		
		return demoConfig.getSetting(name) ;
	}
	
	public void setSetting(Setting setting)
	{
		String demoSettingsPath = env.getProperty("config.json.demo") ;
		SettingsList demoConfig = DemoConfig.getDemoConfig(demoSettingsPath) ;
		
		logger.info("updating demo configuration with setting object",setting);
		// if the setting was successfully set
		if(demoConfig.setSetting(setting)) 
		{
			// update the singletion config object for everyone else
			DemoConfig.updateConfig(demoConfig) ;
			// and write the configuration to file 
			DemoConfig.writeConfig(demoSettingsPath);		
		}
		
	}
}
