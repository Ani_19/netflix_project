package com.openet.odp.service;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import com.openet.odp.Application;
import com.zaxxer.hikari.HikariDataSource;

public class MariaDBConnectionPool {

	private static HikariDataSource dataSource ;
	private static final Properties config = loadProps("/resources/application.properties") ;
	private static final String mariadbUrl = config.getProperty("mariadb.jdbcUrl") ;
	private static final String mariadbUser = config.getProperty("mariadb.user") ;
	private static final String mariadbPassword = config.getProperty("mariadb.password") ;
	
	
	public static HikariDataSource getDataSource()
	{
		if(dataSource == null)
		{
			dataSource = new HikariDataSource();
			dataSource.setMaximumPoolSize(20);
			dataSource.setDriverClassName("org.mariadb.jdbc.Driver");
			dataSource.setJdbcUrl(mariadbUrl);
			dataSource.addDataSourceProperty("user", mariadbUser);
			dataSource.addDataSourceProperty("password", mariadbPassword);
			dataSource.setAutoCommit(false);
		}
		
		return dataSource ;
	}
	
	
	private static Properties loadProps(String path)
	{
		Properties prop = new Properties() ;

		try {
			InputStream input = Application.class.getResourceAsStream(path) ;
			prop.load(input) ;
		} catch (IOException e) {
			e.printStackTrace();
		}

		return prop ;
	}
	
}
