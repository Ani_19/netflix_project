package com.openet.odp.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.openet.odp.spring.model.Snapshot;

@Service
public class SnapshotService {
	
	@Inject
	private Environment env ;
	
	public List<Snapshot> getAllSnapshots() throws SQLException
	{
		List<Snapshot> snapshots = new ArrayList<Snapshot>() ;
		
		Connection connection = DriverManager.getConnection(
				env.getProperty("mariadb.jdbcUrl"),
				env.getProperty("mariadb.user"),
				env.getProperty("mariadb.password")) ;
		
		Statement stmt = connection.createStatement() ;
		ResultSet result = stmt.executeQuery(
				"SELECT snapshot_id, demo_id, username, created_date, vim, vm_image_name FROM snapshots") ;
		
		while(result.next() != false)
		{
			snapshots.add(
					new Snapshot(
							result.getString("snapshot_id"), 
							result.getString("demo_id"), 
							result.getString("username"),
							result.getString("created_date"),
							result.getString("vim"),
							result.getString("vm_image_name")
							) 
					);
		}
		//Close of our connections to the DB
		stmt.close();
		result.close();
		connection.close();
		
		return snapshots ;
	}
	
	public List<Snapshot> getUsersSnapshots(String username) throws SQLException
	{
		List<Snapshot> snapshots = new ArrayList<Snapshot>() ;
		
		Connection connection = DriverManager.getConnection(
				env.getProperty("mariadb.jdbcUrl"),
				env.getProperty("mariadb.user"),
				env.getProperty("mariadb.password")) ;
		
		Statement stmt = connection.createStatement() ;
		ResultSet result = stmt.executeQuery(
				"SELECT snapshot_id, demo_id, username, created_date, vim, vm_image_name FROM snapshots WHERE username = '"+username+"'") ;
		
		while(result.next() != false)
		{
			snapshots.add(
					new Snapshot(
							result.getString("snapshot_id"), 
							result.getString("demo_id"), 
							result.getString("username"),
							result.getString("created_date"),
							result.getString("vim"),
							result.getString("vm_image_name")
							) 
					);
		}
		//Close of our connections to the DB
		stmt.close();
		result.close();
		connection.close();
		
		return snapshots ;
	}
	
	
}
