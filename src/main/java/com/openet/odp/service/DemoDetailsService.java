package com.openet.odp.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.openet.odp.spring.model.DemoDetails ;


@Service
public class DemoDetailsService {
	
	@Inject
	private Environment env ;
	
	public List<DemoDetails> getAllDemoDetails() throws SQLException
	{
		List<DemoDetails> demoDetails = new ArrayList<DemoDetails>() ;
		
		Connection connection = DriverManager.getConnection(
				env.getProperty("mariadb.jdbcUrl"),
				env.getProperty("mariadb.user"),
				env.getProperty("mariadb.password")) ;
		
		Statement stmt = connection.createStatement() ;
		ResultSet result = stmt.executeQuery(
				"SELECT demo_id, description, video_url, resources, title FROM full_demo_details") ;
		
		while(result.next() != false)
		{
			demoDetails.add(
					new DemoDetails(
							result.getString("demo_id"), 
							result.getString("description"), 
							result.getString("video_url"),
							result.getString("resources"),
							result.getString("title")
							) 
					);
		}
		//Close of our connections to the DB
		stmt.close();
		result.close();
		connection.close();
		
		return demoDetails ;
	}
	
	public DemoDetails getDemoDetails(String demo_id) throws SQLException
	{
		
		DemoDetails details = null ;
		
		Connection connection = DriverManager.getConnection(
				env.getProperty("mariadb.jdbcUrl"),
				env.getProperty("mariadb.user"),
				env.getProperty("mariadb.password")) ;
		
		Statement stmt = connection.createStatement() ;
		ResultSet result = stmt.executeQuery(
				"SELECT demo_id, description, video_url, resources, title FROM full_demo_details WHERE demo_id = '"+demo_id+"'") ;
		
		if(result.first() == true)
		{
			details = new DemoDetails(
					result.getString("demo_id"), 
					result.getString("description"), 
					result.getString("video_url"),
					result.getString("resources"),
					result.getString("title")
					);
		}
		//Close of our connections to the DB
		stmt.close();
		result.close();
		connection.close();
		
		return details ;
	}
	
}
