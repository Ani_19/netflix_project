package com.openet.odp.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.openet.odp.spring.model.Log;

@Service
public class LogService {

	@Inject
	private Environment env ;
	
	public List<Log> getAllLogs(int offset , int count) throws SQLException
	{
		List<Log> logs = new ArrayList<Log>() ;
		
		Connection connection = DriverManager.getConnection(
				env.getProperty("mariadb.jdbcUrl"),
				env.getProperty("mariadb.user"),
				env.getProperty("mariadb.password")) ;
		
		Statement stmt = connection.createStatement() ;
		ResultSet result = stmt.executeQuery(
				"SELECT username, timestamp, action, vim FROM logs LIMIT "+count+" OFFSET "+offset) ;
		
		while(result.next() != false)
		{
			logs.add(
					new Log(
							result.getString("username"), //should i be accessing these by index? Possible optimization point? geString is 
							result.getString("timestamp"), //probably much more costly then a index access
							result.getString("action"),
							result.getString("vim")
							) 
					);
		}
		//Close of our connections to the DB
		stmt.close();
		result.close();
		connection.close();
		
		return logs ;
	}
	
	public List<Log> getUserLogs(int offset, int count, String username) throws SQLException
	{
		List<Log> logs = new ArrayList<Log>() ;
		
		Connection connection = DriverManager.getConnection(
				env.getProperty("mariadb.jdbcUrl"),
				env.getProperty("mariadb.user"),
				env.getProperty("mariadb.password")) ;
		
		Statement stmt = connection.createStatement() ;
		ResultSet result = stmt.executeQuery(
				"SELECT username, timestamp, action, vim FROM logs WHERE username = '"+username+"' LIMIT "+count+" OFFSET "+offset) ; 
		
		while(result.next() != false)
		{
			logs.add(
					new Log(
							result.getString("username"), //should i be accessing these by index? Possible optimization point? geString is 
							result.getString("timestamp"), //probably much more costly then a index access
							result.getString("action"),
							result.getString("vim")
							) 
					);
		}
		//Close of our connections to the DB
		stmt.close();
		result.close();
		connection.close();
		
		return logs ;
	}
}
