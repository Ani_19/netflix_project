package com.openet.odp.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.openet.odp.spring.model.Demo;

@Service
public final class DemoService {
	
	@Inject
	private Environment env ;
	
	/**
	 * Querys the MariaDB instance using the values in application.properties for all demo entries
	 * and returns them as a list of demo objects
	 * @return
	 * @throws SQLException
	 */
	public List<Demo> getAllDemos() throws SQLException
	{
		List<Demo> demos = new ArrayList<Demo>() ;
		
		Connection connection = DriverManager.getConnection(
				env.getProperty("mariadb.jdbcUrl"),
				env.getProperty("mariadb.user"),
				env.getProperty("mariadb.password")) ;
		
		Statement stmt = connection.createStatement() ;
		ResultSet result = stmt.executeQuery(
				"SELECT demo_id, title, overview, image_path FROM demos") ; //should the table names be set in the properties file?
		
		while(result.next() != false)
		{
			demos.add(
					new Demo(
							result.getString("demo_id"),
							result.getString("title"),
							result.getString("overview"),
							result.getString("image_path")
							) 
					);
		}
		//Close of our connections to the DB
		stmt.close();
		result.close();
		connection.close();
		
		return demos ;
	}
	
	/**
	 * Querys the MariaDB instance using the values in application.properties for a demo entry 
	 * with a demo_id equal to the id passed in its param
	 * @param demo_id
	 * @return returns a single demo entry if no demo entry is found with the same id returns null
	 * @throws SQLException
	 */
	public Demo getDemo(String demo_id) throws SQLException
	{
		Demo demo = null ;
		Connection connection = DriverManager.getConnection(
				env.getProperty("mariadb.jdbcUrl"),
				env.getProperty("mariadb.user"),
				env.getProperty("mariadb.password")) ;
		
		Statement stmt = connection.createStatement() ;
		ResultSet result = stmt.executeQuery(
				"SELECT demo_id, title, overview, image_path FROM demos WHERE demo_id = '" + demo_id+"'") ;
		
		// if our query returned a result
		if(result.first() == true)
		{
			demo = new Demo(
					result.getString("demo_id"),
					result.getString("title"),
					result.getString("overview"),
					result.getString("image_path")
					) ;
		}
		result.close();
		stmt.close();
		connection.close();
		
		// if the query did not return anything this will be null as per the initalisation of demo
		return demo ;
	}
}
