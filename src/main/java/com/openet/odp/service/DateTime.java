package com.openet.odp.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateTime {
	public static Date getCurrentDateTime(){
		Date now = new Date();
		DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.LONG).format(now);
		return now;
	}
	
	public static String getDBDateTime(){
		 DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd__HH_mm_ss");
		 return dateFormat.format(new Date());
	}
	
	public static DateFormat getDBDateFormat(){
		DateFormat dateForamt = new SimpleDateFormat("yyyy_MM_dd__HH_mm_ss");
		return dateForamt;
	}
}
