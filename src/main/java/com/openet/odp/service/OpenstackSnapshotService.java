package com.openet.odp.service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OpenstackSnapshotService implements Runnable{

	
	private final Logger logger = LoggerFactory.getLogger(OpenstackSnapshotService.class);
	public String ACTIVE_STATUS = "ACTIVE";
	public  Date SERVER_CREATED_TIME;
	public  Date SERVER_SHUTDOWN_TIME;
	public  int SERVER_RUNNING_TIME = 12;
	public int SUC_SUSPEND_CODE = 202;
	public String SERVER_DB_STATUS = null;
	public String serverUUID;
	public String CURRENT_SERVER_STATUS;
	DatabaseService dbService = new DatabaseService();
	OpenstackService openstackSrvc = new OpenstackService();
	

	public OpenstackSnapshotService(){
		
	}
	
	public OpenstackSnapshotService(Date serverShutdownTime){
		SERVER_SHUTDOWN_TIME = serverShutdownTime;
	}
	
			
	public void getServerUUID(){
		logger.info("Getting server UUID from database for suspending");
		ResultSet resultSet = dbService.getServerUUIDFromFIFO();
		try {
			if(resultSet.first() == true)
			{
				serverUUID = resultSet.getString("OP_SERVER_UUID");
				SERVER_DB_STATUS = resultSet.getString("STATUS");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		logger.info("Server UUID: "+serverUUID+ " SERVER_DB_STATUS: "+SERVER_DB_STATUS);
		dbService.deleteUsedUUIDEntry(serverUUID, true);
	}	
	
	public void getCurrentServerStatus(){	
		logger.info("Getting current sever status");
		String serverStatus = openstackSrvc.getServerStatus(serverUUID);
		logger.info("Server Status: "+serverStatus);
		if(serverStatus.contentEquals(ACTIVE_STATUS)){
			suspendActiveServer();
		}else{
			logger.info("Server Status not ACTIVE, so no need to SUSPEND");
		}		
	}
			
	
	private void suspendActiveServer(){
		if(openstackSrvc.suspendActiveServer(serverUUID) == SUC_SUSPEND_CODE){
			logger.info("Server suspended successfully.");
		}else{
			logger.info("Server suspending failed");
		}
		
	}
	
	public void shutdownServer(){
		
		getServerUUID();
		logger.info("SERVER_DB_STATUS : "+SERVER_DB_STATUS);
		if(SERVER_DB_STATUS.contentEquals("RE_LAUNCHED")){
			logger.info("No need to suspend server, exiting this thread");
			return;
		}
		getCurrentServerStatus();
	}
	
	public void run() {
		shutdownServer();
		
		//****************************NEW WORKFLOW**************
		//1) save server timestamp to databse(DONE)
		//2) trigger run method after getting current time and adding 12 hrs
		//3) get the UUID of the first result and dlete that entry
		//4) get the status of the server with that UUID
		//5) if its active, perform following steps, otherwise do nothing
		//6) suspend it 
		
	}

}
