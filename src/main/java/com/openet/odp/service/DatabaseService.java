package com.openet.odp.service;

import java.sql.Connection;
import java.util.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.DateFormat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Service;

import com.openet.odp.config.AppConfig;
import com.openet.odp.spring.model.MariaDBInfo;


@Service
public class DatabaseService {

	private  final Logger logger = LoggerFactory.getLogger(DatabaseService.class);
	public static Connection connection = null;
		
	ApplicationContext ctxx = new AnnotationConfigApplicationContext(AppConfig.class);	   
	MariaDBInfo mariaDBInfo = ctxx.getBean(MariaDBInfo.class);
	private  final String jdbcUrl = mariaDBInfo.getjdbcUrl();
	private  final String user = mariaDBInfo.getUser();
	private  final String password = mariaDBInfo.getPassword();
	
	private final String TO_BE_SUSPENDED = "TO_BE_SUSPENDED";
	private final String RE_LAUNCHED = "RE_LAUNCHED";
	

	private void getDBEnvVariables(){
		logger.info("Getting DB Environment Variables:");
		try{
			connection = DriverManager.getConnection(jdbcUrl, user, password);			
			logger.info(DateTime.getCurrentDateTime().toString()+ ":::Mariadb.jdbcUrl: "+ jdbcUrl);
		}catch(SQLException sqlException){
			logger.info("Unable to get MariaDB EnvironmentVariables.");
			sqlException.printStackTrace();
		}

	}
	
	
	private  Statement connectDatabase(){
		logger.info("Trying Connecting Database.:");
		getDBEnvVariables();
		Statement statement = null;
		try{
			statement = connection.createStatement() ;	
		}catch(SQLException sqlException){
			logger.info("Failed to Connect to database:");
			sqlException.printStackTrace();
		}
		return statement;
	}
	
	public ResultSet getServerUUIDFromFIFO(){
		logger.info("Getting Server UUID from FIFO");
		Statement statement = connectDatabase();
		ResultSet resultSet = null;
		
		String sqlQuery = "SELECT * ";
		sqlQuery += "FROM ";
		sqlQuery += "server_created_time ";
		sqlQuery += "ORDER BY ";
		sqlQuery += "SERVER_CREATED_TIME";
		logger.info("SQL Query for getting UUID from FIFO: "+sqlQuery);
		
		try{
			logger.info("Executing Query:");
			resultSet = statement.executeQuery(sqlQuery);		
		}catch(SQLException sqlException){
			logger.info("SQL Query failed: "+sqlQuery);
			sqlException.printStackTrace();
		}
		logger.info("SQL Query Executed Successfully");
		
		closeConnection(resultSet, statement);
		return resultSet;
		
	}
	
	
	public void deleteEntryFromInstance(String serverUUID){
		logger.info("Deleting UUID entry from Instances");
		Statement statement = connectDatabase();
		
		String sqlQuery = "DELETE FROM instances WHERE UUID=";
		sqlQuery += "'"+serverUUID+"' ";
		sqlQuery += "LIMIT 1";
		
		try{
			logger.info("Executing Query:");
			statement.executeQuery(sqlQuery);	
		}catch(SQLException sqlException){
			logger.info("SQL Query failed: "+sqlQuery);
			sqlException.printStackTrace();
		}
		logger.info("SQL Query Executed Successfully");
		try {
			statement.close();
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void deleteUsedUUIDEntry(String serverUUID, Boolean limit){
		logger.info("Deleting used UUID entry");
		Statement statement = connectDatabase();
		
		String sqlQuery = "DELETE FROM server_created_time WHERE OP_SERVER_UUID=";
		sqlQuery += "'"+serverUUID+"' ";
		sqlQuery += "ORDER BY SERVER_CREATED_TIME";
		if(limit){
			sqlQuery +=	" LIMIT 1";
		}
		
		
		try{
			logger.info("Executing Query:");
			statement.executeQuery(sqlQuery);	
		}catch(SQLException sqlException){
			logger.info("SQL Query failed: "+sqlQuery);
			sqlException.printStackTrace();
		}
		logger.info("SQL Query Executed Successfully");
		try {
			statement.close();
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
	
	public ResultSet getWeaverDetailsForLaunch(String demo_id){
		logger.info("Getting Weaver details from Database:");
		Statement statement = connectDatabase();
		ResultSet result = null;
		
		String  sqlQuery = "SELECT ";
		sqlQuery += "weaver.vnf_id, weaver.tenant_id, weaver.vnf_template_id, weaver.vnfd_id ";
		sqlQuery += "FROM ";
		sqlQuery += "weaver ";
		sqlQuery += "INNER JOIN ";
		sqlQuery += "demos ";
		sqlQuery += "ON ";
		sqlQuery += "weaver.weaver_id = demos.weaver_id ";
		sqlQuery += "WHERE ";
		sqlQuery += "demos.demo_id = '"+demo_id+"'";
		logger.info("SQL Query for getting weaver details for launch: "+sqlQuery);
		
		try{
			logger.info("Executing Query:");
			result = statement.executeQuery(sqlQuery);		
		}catch(SQLException sqlException){
			logger.info("SQL Query failed: "+sqlQuery);
			sqlException.printStackTrace();
		}
		logger.info("SQL Query Executed Successfully");
		logger.info("Returning result: "+result.toString());
		
		closeConnection(result, statement);
	return result;
	}
	
	public void saveInstanceCreation(String instance_id, String username, String instanceName, String demo_id, String vim, String UUID) throws SQLException
	{
		saveServerCreatedTime(UUID);
		
		Statement statement = connectDatabase();
		String sql = "";
		logger.info("Saving instance creation to Database");
		sql = "INSERT INTO logs(username,timestamp,action,vim) VALUES( '"+
				username+"', '"+
				DateTime.getDBDateTime()+"', "+
				"'Launched Instance "+instance_id+" of demo: "+demo_id+"', '"+
				vim+"' )" ;
		
		logger.info(sql);
		statement.addBatch(sql); 
		
		sql = "INSERT INTO instances VALUES( '"+
				instance_id+"', '"+
				demo_id+"', '"+
				username+"', "+
				"NULL , '"+
				instanceName+"', '"+
				DateTime.getDBDateTime()+"', '"+
				vim+"', '"+
				UUID+"')" ;
		
		statement.addBatch(sql);
		
		statement.executeBatch() ;
		
		statement.close(); 
		connection.close();
		logger.info("Instance creation saved successfully to database");
	}
	
	public void updateServerStatusToRelaunch(String serverUUID){
		logger.info("Updating the server status in db to RE_LAUNCHED");
		Statement statement = connectDatabase();
		ResultSet result = null;
		
		String  sqlQuery = "UPDATE ";
				sqlQuery += "server_created_time ";
				sqlQuery += "SET ";
				sqlQuery += "STATUS='"+RE_LAUNCHED+"' ";
				sqlQuery += "WHERE ";
				sqlQuery += "OP_SERVER_UUID='"+serverUUID+"'";
		logger.info("SQL Query for updating status to relaunch: "+sqlQuery);
		
		try{
			logger.info("Executing Query:");
			result = statement.executeQuery(sqlQuery);		
		}catch(SQLException sqlException){
			logger.info("SQL Query failed: "+sqlQuery);
			sqlException.printStackTrace();
		}
		logger.info("SQL Query Executed Successfully");
		
		closeConnection(result, statement);
		
	}
	
	public void saveServerCreatedTime(String UUID){
		logger.info("Saving server created time to database");
		Statement statement = connectDatabase();
		
		String sqlQuery = "INSERT INTO server_created_time VALUES (";
		sqlQuery += "'"+UUID+"', ";
		sqlQuery += "NULL, ";
		sqlQuery += "'"+TO_BE_SUSPENDED+"'";
		sqlQuery += ")";
		
		try {
			statement.addBatch(sqlQuery);
			statement.executeBatch();
			statement.close(); 
			connection.close();
			logger.info("Server created time saved successfully to database");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	public ResultSet getExistingDemoUUIDForUser(String demo_id, String username){
		logger.info("Getting UUID for Demo for User "+username);
		Statement statement = connectDatabase();
		ResultSet result = null;
		
		String sqlQuery = "SELECT ";
				sqlQuery += "UUID ";
				sqlQuery += "FROM instances ";
				sqlQuery += "WHERE ";
				sqlQuery += "demo_id="+"'"+demo_id+"' ";
				sqlQuery += "AND ";
				sqlQuery += "username="+"'"+username+"'";
				
				try{
					logger.info("Executing Query:");
					result = statement.executeQuery(sqlQuery);		
				}catch(SQLException sqlException){
					logger.info("SQL Query failed: "+sqlQuery);
					sqlException.printStackTrace();
				}
				logger.info("SQL Query Executed Successfully");
				logger.info("Returning result: "+result.toString());
				
				closeConnection(result, statement);
			return result;		
		
	}
	
	public void saveInstanceDeletion(String instance_id, String username, String vim) throws SQLException
	{		
		Statement statement = connectDatabase();
		statement.addBatch("INSERT INTO logs(username,timestamp,action,vim) VALUES( '"+
				username+"' , '"+
				DateTime.getDBDateTime()+"', "+
				"'Deleted Instance "+instance_id+"', '"+
				vim+"' )"); 
		
		statement.addBatch("DELETE FROM instances WHERE instance_id = '"+instance_id+"'");
		
		statement.executeBatch() ;
		
		statement.close(); 
		connection.close();
	}
	
	
	private void closeConnection(ResultSet result, Statement statement){
		try{
			logger.info(DateTime.getCurrentDateTime().toString()+ ":::Closing DB connection");
			result.close();
			statement.close();
			connection.close();
		}catch(SQLException sqlException){
			logger.info(DateTime.getCurrentDateTime().toString()+ ":::Error occured while closing DB connection");
			sqlException.printStackTrace();
		}

	}
	
}
