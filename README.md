# README #
### What is this repository for? ###

* This repository contains the source files for netflix_project.

### How do I get set up? ###
Envorment steps for netflix_project:

Steps For Setting MariaDB:
1) Centos 7:[Not tested]
# yum repo set up 
> cd /etc/yum.repos.d/
> vi mariaDB.repo

copy and paste the following: 

		# MariaDB 10.1 CentOS repository list - created 2016-12-20 16:24 UTC
		# http://downloads.mariadb.org/mariadb/repositories/
		[mariadb]
		name = MariaDB
		baseurl = http://yum.mariadb.org/10.1/centos7-amd64
		gpgkey=https://yum.mariadb.org/RPM-GPG-KEY-MariaDB
		gpgcheck=1

		# Install nessarcy files:

		sudo yum install MariaDB-server MariaDB-client wget git 

		# start mariadb
		sudo systemctl start mariadb

		mysql --version

2) ubuntu:
#add mariadb repo 
--> sudo apt-get install software-properties-common
--> sudo apt-key adv --recv-keys --keyserver hkp://keyserver.ubuntu.com:80 0xcbcb082a1bb943db
--> sudo add-apt-repository 'deb http://mirror.jmu.edu/pub/mariadb/repo/5.5/ubuntu trusty main'
--> sudo apt-get update
--> sudo apt-get install mariadb-server
--> sudo service mysql stop
--> sudo mysql_install_db
--> sudo service mysql start
--> sudo mysql_secure_installation

After following these steps if you can log in as root with folowing command(where password is the one which was supplied during installation):
--> mysql -u root -p

However if you cannot then perform the following the steps:
--> sudo mysql -u root

Now inside mysql execute these steps:
-> DROP USER 'root'@'localhost';
-> CREATE USER 'root'@'localhost';
-> GRANT ALL PRIVILEGES ON *.* TO 'root'@'localhost';
-> FLUSH PRIVILEGES;
-> SET PASSWORD FOR 'root'@'localhost' = PASSWORD('your_new_password_for_root');
-> exit;
After exiting from mysql try logging again with following command:

--> mysql -u root -p
where password would be: your_new_password_for_root

steps for setting maven:

>	wget http://www.mirrorservice.org/sites/ftp.apache.org/maven/maven-3/3.3.9/binaries/apache-maven-3.3.9-bin.tar.gz
>	tar -xzvf apache-maven-3.3.9-bin.tar.gz

>	export PATH="path to maven bin directory":$PATH

Import the mariadb sql files

>	cd odp/src/main/resources/

>	mysql -u root < "odp_schema.sql"
>	mysql -u root < "odp_procedures.sql"
>	mysql -u root < "odp_views.sql"
>	mysql -u root < "test_data.sql"
Note: there could be few errors while writing the odp_schema.sql, but there are easy fixes like adding few commans....
verify mariadb setup
	
	mysql -u root
	> use odp;
	> show tables ;

------------------------------------------------------------------------------------------------
Modify the application.properties file for the odp spring service

	vi odp/src/main/resources/application.properties

#Spring boot logging
logging.file=odp.log     # keep same
logging.path=/home/anirudh/ODP/    # path to your directory where project was downloaded
logging.level.org.springframework.web=DEBUG  # keep same 
logging.level.org.hibernate=ERROR # keep same
logging.level.org.openet=DEBUG   # keep same

#maria db connection paramaters
mariadb.jdbcUrl=jdbc:mariadb://localhost:3306/odp   # keep same
mariadb.user=root  		# keep same
mariadb.password=root		# keep same

#json config file locations
config.json.demo=/home/anirudh/ODP/src/main/resources/demoConfig.json		# change path according to your structure

#amazon aws config
aws.endpoint=ec2.us-east-1.amazonaws.com  # keep same
aws.security.groupid=blah					# keep same
aws.security.keypair=POCKey2015		# keep same
aws.security.accesskey=blah			# keep same
aws.security.secretaccesskey=blah			# keep same

#weaver config
weaver.ipAddress=10.0.104.168   # ipaddress of your weavermaster
weaver.port=28050    # keep same

#OPENSTACK CREDENTIALS
openstack.user=admin   # keep same
openstack.password=admin			# keep same
openstack.tenantId=b27eaf3b9b774d5c9d8ca81f28d472f1		# UUID of you instance 
openstack.ipAddress=		# ipaddress for openstack environment
openstack.port=5000		# keep same
openstack.apiVersion=v2.0		# keep same

------------------------------------------------------------------------------------------------------------------------
Steps for Building and running the ODP server jar
>	cd odp/ 
>	mvn clean package
>	cd target/
>	java -jar odp-service.jar

Verify the service compiled correctly:

	http://localhost:8080/login.html

	#to view the API documentation
	http://localhost:8080/swagger-ui.html


Steps for setting up the project in eclipse

	import -> mvn project -> odp

