#!/usr/bin/python

import logging
import os
import sys
import traceback
from ovlm_lib.params import Params
from ovlm_lib import utils
from ovlm_lib.const import Context
if __name__ == "__main__":
    if len(sys.argv) < 2:
        sys.exit('usage: ' + __file__ + ' <PARAMETERS_PATH>')
    PARAMS_OBJ = Params(sys.argv[1])
    OVLM_ROOT = PARAMS_OBJ.get_param('repository_root')
    STAGE_DIR = os.path.join(OVLM_ROOT, Context.STAGE, "")
    PREACTIVE_DIR = os.path.join(OVLM_ROOT, Context.PREACTIVE)
    ACTIVE_DIR = os.path.join(OVLM_ROOT, Context.ACTIVE)
    utils.init_logger(PARAMS_OBJ)
    utils.logging_dump(PARAMS_OBJ.get_dump())
    logging.info("Copy vnf template from stage to the PREACTIVE dir")
    utils.makedirs_if_absent(PREACTIVE_DIR)
    utils.rsync(STAGE_DIR, PREACTIVE_DIR, '-r --delete')
    logging.info("Copy vnf template from stage to the ACTIVE dir")
    utils.makedirs_if_absent(ACTIVE_DIR)
    utils.rsync(STAGE_DIR, ACTIVE_DIR, '-r --delete')
