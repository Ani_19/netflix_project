#!/usr/bin/python
import os
import os.path
import sys
from ovlm_lib import params, utils
if __name__ == "__main__":
        if len(sys.argv) < 2:
                sys.exit('Usage: ' + __file__ + ' YAML_CONFIG_PATH')
        PARAMS = params.Params(sys.argv[1])