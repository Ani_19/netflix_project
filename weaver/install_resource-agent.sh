#!/bin/bash

ARTIFACTORY_URL="http://artifactory.openet-telecom.lan:8081/artifactory/"

download_artifact()
{
	ARTIFACT=$1
    PATH=$2

    wget "${ARTIFACTORY_URL}${PATH}${ARTIFACT}"
}


RA_RPM="resource-agent-rest-impl-1.4.0-x86_64.rpm"
RA_PATH="modules-release-local/com/openet/modules/ovlm/resourceagent/resource-agent-rest-impl/1.4.0/"

FACTER_RPM="facter-2.4.6-1.el7.x86_64.rpm"
FACTER_PATH="ext-release-local/com/facter/facter/2.4.6/"

JRE_TAR="server-jre-8u60-linux-x64.tar.gz"
JRE_PATH="ext-release-local/com/oracle/server-jre/8u60/"

echo "[INFO] Downloading artifacts..."

#download the resource agent rpm
download_artifact($RA_RPM, $RA_PATH)
#donwload the dependancys 
download_artifact($FACTER_RPM, $FACTER_PATH)
download_artifact($JRE_TAR, $JRE_PATH)

#install dependancys
echo "[INFO] Installing dependancies..."
sudo yum -y -q install unzip lksctp-tools.x86_64 libssh ruby pciutils python-jinja2
rpm -i --nosignature $FACTER_RPM

echo "[INFO] Installing Resource agent..."
#install the resource agent
rpm -i --nosignature $RA_RPM

echo "[INFO] Extracting jre to /usr/lib64/ovlm/java ..."

#extract the jre for the resource agent
mdkir /usr/lib64/ovlm/java/
tar -xzvf $JRE_TAR -C /usr/lib64/ovlm/java/ 

echo "[INFO] Enabling the resource agent service..."
#enable the ovlm-ra service
sudo systemctl enable ovlm-ra

echo "[DONE] You can now snapshot this instance!"
